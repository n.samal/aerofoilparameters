import colorsys
import matplotlib.pyplot as plt
import numpy as np

''' Plotting '''

def get_colors(num_colors: int):
    """Generate colors along HSL cylinger

    References:
        Modified from stackoverflow
    """

    colors = []
    hue_max = 330.
    lightness_min = 0.30
    lightness_max = .50
    saturation = 1.0

    for i in range(0, num_colors):  # regular color range
        hue_range = np.linspace(0, hue_max, num_colors)
        hue = hue_range[i] / 360

        lightness = lightness_max

        colors.append(colorsys.hls_to_rgb(hue, lightness, saturation))

    return colors


def plot_extras(axes,
                xlim: tuple = None,
                ylim: tuple = None,
                equal: bool = False,
                title: str = None,
                xlabel: str = None,
                ylabel: str = None,
                show: bool = True):
    """Add additional details to plot axes

    Args:
        show (bool): to plot or not to plot
        equal (bool): use equal axis ratios
    """

    # Add axis labels
    if xlabel:
        axes.set_xlabel(xlabel)
    if ylabel:
        axes.set_ylabel(ylabel)

    # https: // matplotlib.org / api / _as_gen / matplotlib.pyplot.legend.html
    axes.legend(loc='upper left', numpoints=1, prop={'size': 10}, title=title)

    # Add the grid lines
    axes.grid(b=True, which='major', color='grey', linestyle='--', alpha=0.5)
    axes.grid(b=True, which='minor', color='grey', linestyle='--', alpha=0.5)

    if xlim:
        axes.set_xlim(xlim)

    if ylim:
        axes.set_ylim(ylim)

    if equal:
        axes.set_aspect('equal')

    if show:
        plt.show()


''' User Input '''

def get_user_input(prompt: str, valid: set):
    """Get input from the user

    Args:
        prompt (str): text description of query
        valid (set): valid input values
    Returns:
        str: value selection
    """

    value = None

    while value not in valid:
        value = input(prompt)
    return value
