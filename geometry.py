# from abc import ABCMeta, abstractmethod
import matplotlib
from matplotlib import pyplot as plt
import numbers
import numpy as np
import operator
from scipy import interpolate
from typing import List

from observer import Observer, Subject
from utilities import plot_extras, get_user_input

# State descriptors
NORMALS = "normals"
SLOPES = "slopes"
DIST_NORM = "normalized distance"
SPL_C = "spline coefficients"


class Point(Subject):
    def __init__(self, i: numbers.Real, j: numbers.Real, k: numbers.Real = 0.0):
        """Three dimensional point in space

        Args:
            i (numbers.Real): i coordinate value
            j (numbers.Real): j coordinate value
            k (numbers.Real): k coordinate value
        """
        # TODO: add spherical offset, for point generation based upon point

        self.__coords = [None, None, None]  # coordinate values List[float]
        self.__observers = []  # array of observing objects [List]
        self.debug: bool = False  # output flag [bool]

        # Save values
        self.i = i  # coordinate in i dimension [float]
        self.j = j  # coordinate in j dimension [float]
        self.k = k  # coordinate in k dimension [float]

    """ Subject Observer """

    def attach(self, observer: Observer):
        """Add a new observer to the subject"""

        if self.debug:
            print(" Attaching observer")

        self.__observers.append(observer)
        # self.__observers[id(observer)] = observer

    def detach(self, observer: Observer):
        """Remove observer from the subject"""

        if self.debug:
            print(" Detaching observer")

        self.__observers.remove(observer)

    def notify(self):
        """Notify all observers"""

        if self.debug and self.__observers:
            print(" Notifying observers")

        for observer in self.__observers:
            observer.update()

    # TODO: remove this
    @property
    def obs(self):
        return self.__observers

    """ Coordinate Data """

    @property
    def i(self):
        """Get i coordinate

        Returns:
            float: i coordinate
        """
        return self.__coords[0]

    @property
    def j(self):
        """Get j coordinate

        Returns:
            float: i coordinate"""
        return self.__coords[1]

    @property
    def k(self):
        """Get k coordinate

        Returns:
            float: i coordinate"""
        return self.__coords[2]

    @property
    def coords(self) -> list:
        """Get ijk coordinates

        Returns:
            list: ijk coordinates"""

        return self.__coords

    @property
    def coords_hom(self) -> np.array:
        """Get ijk  as homogenous coordinates"""
        x = np.array(self.coords + [1])
        return np.reshape(x, (4, 1))

    @i.setter
    def i(self, val: numbers.Real):
        """Set i coordinate

        Args:
            val (numbers.Real): coordinate value
        """

        if isinstance(val, numbers.Real) is False:
            raise TypeError("Value must be a numerical type")

        self.__coords[0] = val
        self.notify()

    @j.setter
    def j(self, val: numbers.Real):
        """Set j coordinate

        Args:
            val (numbers.Real): coordinate value
        """

        if isinstance(val, numbers.Real) is False:
            raise TypeError("Value must be a numerical type")

        self.__coords[1] = val
        self.notify()

    @k.setter
    def k(self, val: numbers.Real):
        """Set k coordinate

        Args:
            val (numbers.Real): coordinate value
        """

        if isinstance(val, numbers.Real) is False:
            raise TypeError("Value must be a numerical type")

        self.__coords[2] = val
        self.notify()

    @coords.setter
    def coords(self, arr):
        """Set all coordinates at once

        Args:
            arr (iterable): coordinate values in each direction
        """
        if hasattr(arr, "__iter__") is False:
            raise TypeError("Value must be iterable")
        if len(arr) != 3:
            raise ValueError("Iterable must contain 3 values")

        for ix, val in enumerate(arr):

            if isinstance(val, numbers.Real) is False:
                raise TypeError("Value must be a numerical type")

            self.__coords[ix] = val

        self.notify()

    """ Properties: general """

    def __str__(self) -> str:
        """Console output for object"""
        return f"[{self.i:.3f}, {self.j:.3f}, {self.k:.3f}]"

    def __repr__(self) -> str:
        """Object representation"""
        return f"Point({self.i:.3f}, {self.j:.3f}, {self.k:.3f})"

    def __getitem__(self, ix: int):
        """Grab value at coordinate index

        Args:
            ix (int): coordinate index
        """

        if isinstance(ix, int) is False:
            raise TypeError("Index must be an integer")

        return self.__coords[ix]

    def __len__(self) -> int:
        """Return length of point coordinates"""
        return len(self.coords)

    """ Properties: comparison """

    def __general_bool_operator(self, other, op) -> List[bool]:
        """Apply a comparison operator

        Args:
            other (Point): point data to
            op (function): operator function to apply
        """
        if isinstance(other, Point) is False:
            raise TypeError("Value must be Point object")

        return list(map(op, self, other))

    def __lt__(self, other) -> List[bool]:
        """Compare two points: less than

        Args:
            other (Point): point in space
        Returns:
            List[bool]
        """
        return self.__general_bool_operator(other, operator.lt)

    def __le__(self, other) -> List[bool]:
        """Compare two points: less than or equal

        Args:
            other (Point): point in space
        Returns:
            List[bool]
        """
        return self.__general_bool_operator(other, operator.le)

    def __gt__(self, other) -> List[bool]:
        """Compare two points: greater than

        Args:
            other (Point): point in space
        Returns:
            List[bool]
        """
        return self.__general_bool_operator(other, operator.gt)

    def __ge__(self, other) -> List[bool]:
        """Compare two points: greater than or equal

        Args:
            other (Point): point in space
        Returns:
            List[bool]
        """
        return self.__general_bool_operator(other, operator.ge)

    def __eq__(self, other) -> bool:
        """Compare two points: equal

        If the points are within a tolerance we consider them the same

        Args:
            other (Point): point in space
        Returns:
            List
        """

        tol = 1e-5

        if isinstance(other, Point):
            return self.dist(other) <= tol

        # Data is not of the correct type
        else:
            return False

    def __ne__(self, other) -> bool:
        """Compare two points: not equal

        If the points further than our tolerance then they aren't equal

        Args:
            other (Point): point in space
        Returns:
            List
        """
        return not self.__eq__

    """ Properties: math """

    def __general_math_operator(self, other, op):
        """Apply a general operator

        Args:
            other (Point): point object
            op (function): operator function to apply
        """

        if isinstance(other, Point) is False:
            raise TypeError("Value must be a Point object")

        args = map(op, self, other)

        return Point(*args)

    def __add__(self, other):
        """Add two points

        Args:
            other (Point): point in space
        """
        return self.__general_math_operator(other, operator.add)

    def __sub__(self, other):
        """Subtract two points

        Args:
            other (Point): point in space
        """
        return self.__general_math_operator(other, operator.sub)

    def __mul__(self, other):
        """Multiply two points

        Args:
            other (Point): point in space
        """
        return self.__general_math_operator(other, operator.mul)

    def __div__(self, other):
        """Divide two points

        Args:
            other (Point): point in space
        """
        return self.__general_math_operator(other, operator.truediv)

    """ Distance"""

    def dist(self, other) -> float:
        """Compute 3D distance between points

        Args:
            other (Point): point in space
        Returns:
            float: distance in 3D space
        """
        return self.__dist_general(other, [0, 1, 2])

    def __dist_general(self, other, dirs: List[int]) -> float:
        """Compute distance in specified directions

        distance = sqrt( (x1 - x2)^2 + ... (z1 - z2)^2)

        Args:
            other (Point): point in space
            dirs: indices of coordinate directions
        Returns:
            float: distance
        Notes:
            methods modified to avoid overflow
        """

        return np.sqrt(np.sum(np.square(self - other)[dirs]))

    """ Distance: 1D """

    def dist_i(self, other) -> float:
        """Compute 1D distance in i direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [0])

    def dist_j(self, other) -> float:
        """Compute 1D distance in j direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [1])

    def dist_k(self, other) -> float:
        """Compute 1D distance in k direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [2])

    """ Distance: 2D """

    def dist_ij(self, other) -> float:
        """Compute 2D distance in ij directions

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [0, 1])

    def dist_ik(self, other) -> float:
        """Compute 1D distance in ik directions

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [0, 2])

    def dist_jk(self, other) -> float:
        """Compute 1D distance in k direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__dist_general(other, [1, 2])

    """ Slope """

    def __slope_general(self, other, ix: int, jx: int):
        """Compute slope between two coordinate directions

        slope = (y2 - y1) / (x2 - x1)

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """

        if isinstance(other, Point) is False:
            raise TypeError("Value must be Point object")

        return (other[jx] - self[jx]) / (other[ix] - self[ix])

    def slope_ij(self, other) -> float:
        """Compute slope in ij direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 0, 1)

    def slope_ik(self, other) -> float:
        """Compute slope in ik direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 0, 2)

    def slope_ji(self, other) -> float:
        """Compute slope in ji direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 1, 0)

    def slope_jk(self, other) -> float:
        """Compute slope in jk direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 1, 2)

    def slope_ki(self, other) -> float:
        """Compute slope in ki direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 2, 0)

    def slope_kj(self, other) -> float:
        """Compute slope in kj direction

        Args:
            other (Point): point in space
        Returns:
            float: distance
        """
        return self.__slope_general(other, 2, 1)

    """ Transforms: Matrices """

    @staticmethod
    def __get_i_rotation_matrice(angle: numbers.Real) -> np.array:
        """Get the rotation matrice about i axis

        Args:
            angle (numbers.Real): amount of rotation in radians
        Returns:
            np.array: rotation matrix
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """

        r = [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, np.cos(angle), -np.sin(angle), 0.0],
            [0.0, np.sin(angle), np.cos(angle), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]
        return np.array(r)

    @staticmethod
    def __get_j_rotation_matrice(angle: numbers.Real) -> np.array:
        """Get the rotation matrice about j axis

        Args:
            angle (numbers.Real): amount of rotation in radians
        Returns:
            np.array: rotation matrix
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """

        r = [
            [np.cos(angle), 0.0, np.sin(angle), 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [-np.sin(angle), 0.0, np.cos(angle), 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]
        return np.array(r)

    @staticmethod
    def __get_k_rotation_matrice(angle: numbers.Real) -> np.array:
        """Get the rotation matrice about k axis

        Args:
            angle (numbers.Real): amount of rotation in radians
        Returns:
            np.array: rotation matrix
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """

        r = [
            [np.cos(angle), -np.sin(angle), 0.0, 0.0],
            [np.sin(angle), np.cos(angle), 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]
        return np.array(r)

    @staticmethod
    def __get_translation_matrice(i: numbers.Real, j: numbers.Real, k: numbers.Real):
        """Get the translation matrice

        i_new = i_old + offset_i
        j_new = j_old + offset_j
        k_new = k_old + offset_k

        Args:
            i (numbers.Real): offset in i direction
            j (numbers.Real): offset in j direction
            k (numbers.Real): offset in k direction
        Returns:
            nd.array: translation matrice
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        r = [
            [1.0, 0.0, 0.0, i],
            [0.0, 1.0, 0.0, j],
            [0.0, 0.0, 1.0, k],
            [0.0, 0.0, 0.0, 1.0],
        ]
        return np.array(r)

    @staticmethod
    def __get_scaling_matrice(i: numbers.Real, j: numbers.Real, k: numbers.Real):
        """Get the scaling matrice

        This assume we are at the center of scale

        i_new = i_old * i_scale
        j_new = j_old * j_scale
        k_new = k_old * k_scale

        Args:
            i (numbers.Real): offset in i direction
            j (numbers.Real): offset in j direction
            k (numbers.Real): offset in k direction
        Returns:
            nd.array: translation matrice
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        r = [
            [i, 0.0, 0.0, 0.0],
            [0.0, j, 0.0, 0.0],
            [0.0, 0.0, k, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ]
        return np.array(r)

    """ Transforms: General """

    def apply_transformation(self, t: np.array, center=None):
        """Apply a transformation matrice to the current coordinates

        Args:
            t (np.array): transformation matrice
            center (Point): center point for the transformation
        Returns:
            Point: transformed point
        References:
            https://math.stackexchange.com/questions/2093314/rotation-matrix-of-rotation-around-a-point-other-than-the-origin
            https://www.migenius.com/articles/3d-transformations-part1-matrices
            https://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
            http://www.euclideanspace.com/maths/geometry/affine/aroundPoint/
            https://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
        """

        # Assume we're at center, apply transform via matrix multiplication
        if center is None:
            xp = np.matmul(t, self.coords_hom).flatten()

        elif isinstance(center, Point) is False:
            raise TypeError("Center of rotation must be Point object")
        else:
            # Apply center transforms then apply transforms
            return self.apply_transformations([t], center)

        # Extract coordinates
        return Point(*xp[0:3])

    def apply_transformations(self, t: list, center=None):
        """Apply a series of transformation matrices to the current coordinates

        Args:
            t (list): transformation matrices to apply in correct order
            center (Point): center point for the transformation
        Returns:
            Point: transformed point
        """

        # Assume we're at center
        if center is None:
            transforms = t

        elif isinstance(center, Point) is False:
            raise TypeError("Center of rotation must be Point object")

        else:

            # Get translations to center and back to normal
            t1 = self.__get_translation_matrice(-center.i, -center.j, -center.k)
            t3 = self.__get_translation_matrice(center.i, center.j, center.k)

            # Add center point transforms before and after
            transforms = [t1] + t + [t3]

        # Apply transforms
        # can't use reduce due to argument order
        xp = None

        for ti in transforms:
            if xp is None:
                xp = np.matmul(ti, self.coords_hom)
            else:
                xp = np.matmul(ti, xp)

        # For ease
        xp = xp.flatten()

        # Extract coordinates
        return Point(*xp[0:3])

    """ Transforms: Rotation """

    def rotate_ijk(self, angles, center=None):
        """Apply a series of rotations for ijk

        Args:
            angles (iterable): amount of rotation in each direction [radians]
            center (Point): center of rotation
        Returns:
            Point: rotated point
        References:
            https://en.wikipedia.org/wiki/Rotation_of_axes
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        Notes:
            Order of operations will matter
        """

        # Define rotation matrices
        rx = self.__get_i_rotation_matrice(angles[0])
        ry = self.__get_j_rotation_matrice(angles[1])
        rz = self.__get_k_rotation_matrice(angles[2])

        return self.apply_transformations([rx, ry, rz], center)

    def rotate_i(self, angle: float, center=None):
        """Rotate the point by an i axis in jk plane

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Point: rotated point
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        # Generate rotation and apply
        return self.apply_transformation(self.__get_i_rotation_matrice(angle), center)

    def rotate_j(self, angle: float, center=None):
        """Rotate the point by an j axis in ik plane

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Point: rotated point
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        # Generate rotation and apply
        return self.apply_transformation(self.__get_j_rotation_matrice(angle), center)

    def rotate_k(self, angle: float, center=None):
        """Rotate the point by an k axis in ij plane

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Point: rotated point
        References:
            https://www.tutorialspoint.com/computer_graphics/3d_transformation.htm
        """
        # Generate rotation and apply
        return self.apply_transformation(self.__get_k_rotation_matrice(angle), center)

    """ Transformations: Miscellaneous """

    def reflect_i(self, center=None):
        """Reflect the current point about i and the center point

        Args:
            center (Point): center of rotation
        Returns:
            Point: reflected point
        """
        return self.scale([-1.0, 1.0, 1.0], center)

    def reflect_j(self, center=None):
        """Reflect the current point about j and the center point

        Args:
            center (Point): center of rotation
        Returns:
            Point: reflected point
        """
        return self.scale([1.0, -1.0, 1.0], center)

    def reflect_k(self, center=None):
        """Reflect the current point about k and the center point

        Args:
            center (Point): center of rotation
        Returns:
            Point: reflected point
        """
        return self.scale([1.0, 1.0, -1.0], center)

    def scale(self, scale: numbers.Real = 1.0, center=None):
        """Scale the point about a central point

        i_new = (i_old - i_cen)* i_scale + i_cen
        j_new = (j_old - j_cen)* j_scale + j_cen
        k_new = (k_old - k_cen)* k_scale + k_cen

        Offset the coordinates by the center point.
        Scale the point, then add the offset back

        Args:
            center (Point): central point for scaling
            scale (numbers.Real or iterable): scale factor(s)
                                          ie: 2.0 or [2.2, 1.5, 1.0]
        Returns:
            Point: scaled point object
        References:
            https://math.stackexchange.com/questions/2093314/rotation-matrix-of-rotation-around-a-point-other-than-the-origin
        """

        # Singular value: use uniform scaling
        if hasattr(scale, "__len__") is False:
            scale_i, scale_j, scale_k = scale, scale, scale

        # Iterable: use unique scaling in each direction
        elif hasattr(scale, "__iter__") and len(scale) == 3:
            scale_i, scale_j, scale_k = scale

        else:
            raise ValueError(
                "Expecting a singular scale value or scales provided for i, j, k directions"
            )

        # Generate scaling matrix and apply
        return self.apply_transformation(
            self.__get_scaling_matrice(scale_i, scale_j, scale_k), center
        )

    def translate(
        self, i: numbers.Real = 0.0, j: numbers.Real = 0.0, k: numbers.Real = 0.0
    ):
        """Offset the point

        i_new = i_old + offset_i
        j_new = j_old + offset_j
        k_new = k_old + offset_k

        Args:
            i (numbers.Real): offset in i direction
            j (numbers.Real): offset in j direction
            k (numbers.Real): offset in k direction
        Returns:
            Point: Translated point object
        """
        t = self.__get_translation_matrice(i, j, k)
        return self.apply_transformation(t)

    """ Vector Operations """

    def mag(self) -> float:
        """Compute the magnitude of the current point

        magnitude = sqrt(x^2 + y^2 + z^2)

        Returns
            float: magnitude of directions
        Notes:
            methods modified to avoid overflow
        """
        return np.sqrt(np.sum(np.square(self.coords)))


class Curve(Observer):
    """General curve of points in 2D/3D space"""

    def __init__(self, points: List[Point]):

        # TODO: build min and max coordinates into curve by dimension, we update these with any update to a point
        # TODO: add closed parameter (if start ~= stop)

        if hasattr(points, "__iter__") is False:
            raise TypeError("Value must be iterable")

        # Initialization
        self.__debug: bool = False  # flag for console output
        self.__points = [None for _ in range(len(points))]  # List[Point]
        self.__i = [None for _ in range(len(points))]  # List[float]
        self.__j = [None for _ in range(len(points))]  # List[float]
        self.__k = [None for _ in range(len(points))]  # List[float]
        self.__s = None  # List[float]
        self.__tck_arr = [None, None, None]  # List[Tuple]

        # Run checks on each point
        for ix in range(len(points)):

            # Store coordinates for easy access
            self.points[ix] = points[ix]
            self.__i[ix] = points[ix].i
            self.__j[ix] = points[ix].j
            self.__k[ix] = points[ix].k

            # Attach subject (point) to the observer (Curve)
            self.points[ix].attach(self)

    @classmethod
    def from_file(cls, filepath: str, delim=None):
        """Create a curve from a text file of coordinate values

        Args:
            filepath (str): location of file to load
            delim (str): delimiting character
        Returns:
            Curve: array of points in 3d space
        """

        trans_table = str.maketrans({char: None for char in ["\r", "\n"]})
        points: list = []

        # Load file
        with open(filepath) as file_obj:

            for line in file_obj:

                # Clean line
                line = line.translate(trans_table)
                line = line.strip(" ")

                try:

                    # Split string by delimeter
                    line = line.split(delim)

                    # Convert to floats
                    pt = list(map(float, line))

                    # Create a point
                    points.append(Point(*pt))

                except:
                    pass

        return cls(points)

    @classmethod
    def from_values(cls, itrb):
        """Create a curve from iterable of coordinate values

        Args:
            itrb (array like): array of coordinate values
        Returns:
            Curve: array of points in 3d space
        """

        points: list = []

        for pt in itrb:

            # Create point
            points.append(Point(*pt))

        return cls(points)

    """  Subject Observer """

    def update(self):
        """Object requires an update"""

        if self.__debug:
            print(" Subject update: Curve")

        # Reset precomputed values
        self.__s = None
        self.__tck_arr = [None, None, None]

    def get_state(self) -> dict:
        """Get state information on the current observer

        Returns:
            Dict[str, str]: class type for each variable
        """

        # Get class name for spline coefficients
        if not any(self.__tck_arr):
            tck_type = None.__class__.__name__
        else:
            tck_type = self.__tck_arr.__class__.__name__

        return {DIST_NORM: self.__s.__class__.__name__, SPL_C: tck_type}

    """ Properties: general """

    def __getitem__(self, key):
        """Grab point(s) for the specified indices

        Args:
            key (int/slice): point indice(s)
        Returns:
            Point: point along the curve index
            Curve: points along the curve indices
        """

        if isinstance(key, int):
            return self.__points[key]
        elif isinstance(key, slice) is True:

            # Define defaults [start, stop, step]
            args = [0, key.stop, 1]

            # Define slice defaults
            if key.start is not None:
                args[0] = key.start
            if key.step is not None:
                args[2] = key.step

            pts = [self.__points[ix] for ix in range(*args)]

            if len(pts) == 2:

                if self.dim == 2:
                    return Line2D(pts)
                else:
                    return Line3D(pts)
            else:
                return self.__class__(pts)
        else:
            raise TypeError("Index must be an integer or a slice")

    def __setitem__(self, ix: int, pt: Point) -> Point:
        """Set point at specified index

        Args:
            ix (int): point index
            pt (Point): point object
        """
        # TODO: extend functionality to slices

        if isinstance(ix, int) is False:
            raise TypeError("Index must be an integer")

        if isinstance(pt, Point) is False:
            raise TypeError("pt must be a Point object")

        # Remove old subject from observer
        self.__points[ix].detach(self)

        # Update indices
        self.__points[ix] = pt
        self.__i[ix] = pt.i
        self.__j[ix] = pt.j
        self.__k[ix] = pt.k

        # Attach new subject to this observer
        self.__points[ix].attach(self)
        self.update()

    def __len__(self) -> int:
        """Get the number of points in the curve"""
        return len(self.__points)

    def __str__(self):
        """String representation"""
        return "[{}]".format(", ".join(map(str, self.points)))

    def __repr__(self):
        """Object representation"""
        return "{}([{}])".format(
            self.__class__.__name__, ", ".join(map(repr, self.points))
        )

    """ Properties: values """

    @property
    def points(self) -> List[Point]:
        """Get the point cloud defining the curve"""
        return self.__points

    @points.setter
    def points(self, points: List[Point]):
        """Redefine all points

        Args:
            points (List[Point]): array of point objects
        Todo:
            - make this method work
        """

        if hasattr(points, "__iter__") is False:
            raise TypeError("Points array must be an iterable")

        for ix in range(len(points)):
            self[ix] = points[ix]

    @property
    def i(self) -> List[numbers.Real]:
        """Get i coordinate for all points"""
        return self.__i

    @property
    def j(self) -> List[numbers.Real]:
        """Get j coordinate for all points"""
        return self.__j

    @property
    def k(self) -> List[numbers.Real]:
        """Get k coordinate for all points"""
        return self.__k

    @property
    def s(self) -> List[numbers.Real]:
        """Return normalized distance along curve"""
        return self.get_normalized_s_distance()

    @property
    def start(self) -> Point:
        """Grab the first point in the curve"""
        return self.points[0]

    @start.setter
    def start(self, point: Point):
        """Set the first point"""
        self[0] = point

    @property
    def stop(self) -> Point:
        """Grab the last point in the curve"""
        return self[-1]

    @stop.setter
    def stop(self, point: Point):
        """Set the last point"""
        self.points[-1] = point

    @property
    def dim(self) -> int:
        """Number of key dimensions"""
        # All equal = 0, Different Values = 1
        equal_i = not all([self.start.i == i for i in self.i])
        equal_j = not all([self.start.j == j for j in self.j])
        equal_k = not all([self.start.k == k for k in self.k])

        return sum([equal_i, equal_j, equal_k])

    @staticmethod
    def get_dim_index(dim: str) -> int:
        """Grab dimension index for the given character

        Args:
            dim (str): dimension coordinate ie: 'i'
        Returns:
            int: indice of dimension
        """

        d = {"i": 0, "j": 1, "k": 2}

        if dim not in d:
            raise ValueError(
                "Invalid dimension specified: '{dim}'. Valid dimensions include (i, j, k)"
            )
        return d[dim]

    """ Properties: Fit """

    def get_tck(self, dim: str):
        """Get spline tuple for each dimension

        Args:
            dim (str): coordinate of interest
        Returns:
            Tuple: knots, coefficients, and degree of spline
        References:
            https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splprep.html#scipy.interpolate.splprep
        """

        # Grab current fit
        tck = self.__tck_arr[self.get_dim_index(dim)]

        # Calculate fit if not available
        if tck is None:
            self.fit_spline(dim)

        return self.__tck_arr[self.get_dim_index(dim)]

    def set_tck(self, dim: str, tck: tuple):
        """Set spline tuple for each dimension

        Args:
            dim (str): dimension coordinate ie: 'i'
            tck (tuple): knots, coefficients, and degree of spline
        Returns:
        References:
            https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splprep.html#scipy.interpolate.splprep
        """
        self.__tck_arr[self.get_dim_index(dim)] = tck

    @property
    def tck_array(self):
        """Get B spline tuple for all dimensions

        Returns:
            List[Tuple]: knots, coefficients and spline degree for each
                         dimension
        """
        # No spline parameters available
        if not any(self.__tck_arr):
            self.fit_splines()

        return self.__tck_arr

    def fit_spline(self, dim: str, show: bool = False, **kwargs):
        """Fit a B spline to the specified dimension

        Args:
            dim (str): dimension coordinate ie: 'i'
            show (bool): visualize the fit
        Returns:
            None
        """
        # TODO: interpolate points using a non-uniform weighted distribution

        if self.__debug:
            print(f" Fitting spline: {dim}")

        # Interpolate all points in current dimension with normalized parameter
        s_norm = self.get_normalized_s_distance()
        tck = interpolate.splrep(s_norm, getattr(self, dim), s=0, **kwargs)

        if show:
            y_hat = interpolate.splev(s_norm, tck, der=0)
            s_range = np.linspace(0, 1.0, 100)

            axes = plt.gca()
            axes.plot(
                s_norm, getattr(self, dim), label="raw", color="black", marker="o"
            )
            axes.plot(s_range, y_hat, label="spline", color="green", marker=None)

            # Add extras
            plot_extras(
                axes,
                xlabel="Normalized Distance, s[-]",
                ylabel=f"Dimension, {dim} [-]",
                show=True,
            )

        # Save fit
        self.set_tck(dim, tck)

    def fit_splines(self, **kwargs):
        """Fit a B spline to all available dimensions"""

        for dim in ["i", "j", "k"]:
            self.fit_spline(dim, **kwargs)

    """ Methods """

    def angle(self, show: bool = False):
        """Compute the angle between vectors

        The angle between vectors can be calculated using the dot product.
        We know the dot product is a projection of one vector onto another.
        We also know that the cosine has a very similar relationship
        ie: A basic triangle. A*cos(theta) = B

        Args:
            show (bool): plot the calculated distribution
        Returns:
            List[numbers]: angle between line segments (radians)
        References:
            https://www.wikihow.com/Find-the-Angle-Between-Two-Vectors
            https://math.stackexchange.com/questions/2610186/discrete-points-curvature-analysis#2620690
            https://en.wikipedia.org/wiki/Direction_cosine
        """

        theta = [None for ix in range(len(self))]

        for ix in range(1, len(self) - 1):

            # Vectors adjacent to point
            zkm1 = self[ix - 1] - self[ix]
            zkp1 = self[ix + 1] - self[ix]

            # Angle projection
            num = np.dot(zkm1.coords, zkp1.coords)
            den = zkm1.mag() * zkp1.mag()

            # Compute angle between vectors
            theta[ix] = np.arccos(num / den)

        if show:

            range_i = list(range(len(self)))

            fig, axes = plt.subplots(2, 1)

            # Plot 1: airfoil
            axes[0] = self.plot(axes[0], color="black", label="Curve")
            plot_extras(
                axes[0],
                xlabel="Dimension i",
                ylabel="Dimension j",
                equal=True,
                show=False,
            )

            # Plot 2: angular distribution
            axes[1].plot(range_i, theta, label="Angle")
            plot_extras(axes[1], xlabel="Index, ix [-]", ylabel="Angle, theta [rad]")

        return theta

    def reverse(self):
        """Reverse the order of the points"""
        self.points = self.points[::-1]

    """ Transformations """

    def __general_transform(self, method: str, *args, **kwargs):
        """Apply arbitrary method to points in curve

        Args:
            method (str): function to apply to objects
        Returns:
            Curve: modified curve
        References:
            https://docs.python.org/3.6/library/functions.html
            https://stackoverflow.com/questions/4075190/what-is-getattr-exactly-and-how-do-i-use-it
            https://stackoverflow.com/questions/2682012/how-to-call-same-method-for-a-list-of-objects
        """
        points = [
            getattr(self.points[ix], method)(*args, **kwargs) for ix in range(len(self))
        ]

        # We got points!
        if points != [None for _ in range(len(self))]:
            return self.__class__(points)

    def translate(self, *args, **kwargs):
        """Apply a translation to all points

        Args:
            i (numbers.Real): offset in i direction
            j (numbers.Real): offset in j direction
            k (numbers.Real): offset in k direction
        Returns:
            Curve: curve of translated points
        """
        return self.__general_transform("translate", *args, **kwargs)

    def scale(self, *args, **kwargs):
        """Scale all points in the curve about a central point

        i_new = (i_old - i_cen)* i_scale + i_cen
        j_new = (j_old - j_cen)* j_scale + j_cen
        k_new = (k_old - k_cen)* k_scale + k_cen

        Offset the coordinates by the center point.
        Scale the point, then add the offset back

        Args:
            center (Point): central point for scaling
            scale_i (numbers.Real or iterable): scale factor(s) ie: 2.0 or [2.2, 1.5, 1.0]
        Returns:
            Curve: curve of scaled point objects
        """
        return self.__general_transform("scale", *args, **kwargs)

    def reflect_i(self, *args, **kwargs):
        """Reflect all points in the curve about the i

        Args:
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("reflect_i", *args, **kwargs)

    def reflect_j(self, *args, **kwargs):
        """Reflect all points in the curve about the j

        Args:
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("reflect_j", *args, **kwargs)

    def reflect_k(self, *args, **kwargs):
        """Reflect all points in the curve about the k

        Args:
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("reflect_k", *args, **kwargs)

    """ Transformations: Rotations """

    def rotate_i(self, *args, **kwargs):
        """Rotate all points in the curve about the i axis

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("rotate_i", *args, **kwargs)

    def rotate_j(self, *args, **kwargs):
        """Rotate all points in the curve about the j axis

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("rotate_j", *args, **kwargs)

    def rotate_k(self, *args, **kwargs):
        """Rotate all points in the curve about the k axis

        Args:
            angle (float): amount of rotation in radians
            center (Point): center of rotation
        Returns:
            Curve: curve of rotated point objects
        """
        return self.__general_transform("rotate_k", *args, **kwargs)

    """ Interpolate """

    def get_normalized_s_distance(self):
        """Compute the normalized distance between points on a curve

        Returns:
            np.array: array of normalized distance along the curve
        """

        if self.__s is None:

            if self.__debug:
                print(" Computing normalized distance")

            n = len(self)  # number of points in raw data
            s = np.zeros(n)  # cummulative distance between points

            # Compute cummulative distance
            for ix in range(1, n):

                # Add to previous cummulative distance
                s[ix] = s[ix - 1] + self[ix].dist(self[ix - 1])

            # Normalize by max cum sum
            self.__s = s / s[-1]

        return self.__s

    def interpolate_by_s(self, n: int = 100, show: bool = False):
        """Interpolate curve coordinates using normalized distance

        Args:
            n (int): number of points for interpolation
            show (bool): show the interpolated values vs the original
        Returns:
            Curve: interpolated points
        """

        # Define interpolation quantites
        s_range = np.linspace(0, 1, n)  # interpolation locations
        pts = [Point(0.0, 0.0, 0.0) for ix in range(n)]  # interpolated points

        # Go through each dimensional coordinate
        for dim in ["i", "j", "k"]:

            # Interpolate all points in current dimension with normalized value
            tck = self.get_tck(dim)
            intp = interpolate.splev(s_range, tck, der=0)

            # Save interpolated dimensional values
            for ix in range(n):
                setattr(pts[ix], dim, intp[ix])

            if show:

                axes = plt.gca()
                axes.plot(
                    self.s,
                    getattr(self, dim),
                    label="Original",
                    marker="o",
                    color="grey",
                )
                axes.plot(
                    s_range,
                    intp,
                    label="Interpolated",
                    color="green",
                    linestyle="-.",
                    lw=3,
                )
                plot_extras(axes, title=f"Dimension: {dim}", equal=False, show=True)

        return self.__class__(pts)

    """ Utility """

    def plot(self, axes=None, var_x: str = "i", var_y: str = "j", **kwargs):
        """Plot the specified curve dimensions in 2D

        Args:
            axes (matplotlib.axes.Axes): axes for plot
            var_x (str): variable on the x axis
            var_y (str): variable on the y axis
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        if axes is None:
            axes = plt.gca()

        # Plot defined variables
        axes.plot(getattr(self, var_x), getattr(self, var_y), **kwargs)
        return axes


class Curve2D(Curve):
    """Curve defined in 2D space"""

    def __init__(self, points: List[Point]):

        super().__init__(points)

        # Initialize
        self.__arr_m = None  # List[List[float] slopes/tangents in each dimension
        self.__arr_n = None  # List[List[float] normals in each dimension

    """ Subject Observer """

    def update(self):
        """Object requires an update"""

        # Reset precomputed values in parent
        super().update()

        # Update object vars
        self.__arr_m = None  # slope
        self.__arr_n = None  # normals

    def get_state(self) -> dict:
        """Get state information on the current observer

        Returns:
            Dict[str, str]: class type for each variable
        """
        return {
            **super().get_state(),
            **{
                SLOPES: self.__arr_n.__class__.__name__,
                NORMALS: self.__arr_n.__class__.__name__,
            },
        }

    """ Properties: General """

    def __str__(self):
        """String representation"""
        return "[{}]".format(", ".join(map(str, self.points)))

    def __repr__(self):
        """Object representation"""
        return "{}([{}])".format(
            self.__class__.__name__, ", ".join(map(repr, self.points))
        )

    """ Properties: values """

    @property
    def normals(self):
        """Calculate normals at each point"""

        if self.__arr_n is None:
            self.__calculate_slopes_and_normals()
        return self.__arr_n

    @property
    def slopes(self):
        """Grab slopes at each point"""

        if self.__arr_m is None:
            self.__calculate_slopes_and_normals()
        return self.__arr_m

    @property
    def tangents(self):
        """Grab slopes at each point"""
        return self.slopes

    """ Normals & Tangents """

    def __calculate_slopes_and_normals(self) -> np.array:
        """Calculate the slopes and normals at each point

        Returns
            List[List[float]]: slopes in each direction for each point
        """
        # TODO: create unit tangent for normal vector calculation

        # Get the spline parameters
        arr_s = self.s
        arr_tck = self.tck_array

        # Overwrite old values
        self.__arr_m = [[None, None, None] for _ in range(len(self))]
        self.__arr_n = [[None, None, None] for _ in range(len(self))]

        # At every point
        for ix in range(len(self)):

            # Compute derivatives in each direction
            self.__arr_m[ix] = np.array(
                [interpolate.splev(arr_s[ix], tck, der=1) for tck in arr_tck]
            )
            self.__arr_n[ix] = np.array(
                [interpolate.splev(arr_s[ix], tck, der=2) for tck in arr_tck]
            )

            # Normalize by magnitude
            # self.__arr_n[ix] = self.__arr_n[ix] / np.sqrt(np.sum(np.square(self.__arr_n[ix])))

    def draw_curvature_normal_lines(self, scalar: float = 1.0, axes=None, **kwargs):
        """Generate normal lines along the curve to display curavature

        Args:
            scalar (float): multiplier on curvature value
            axes (matplotlib.axes.Axes): axes for plot
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        k = self.curvature()

        if axes is None:
            axes = plt.gca()

        if "color" not in kwargs:
            norm = matplotlib.colors.Normalize(
                vmin=np.min(k), vmax=np.max(k), clip=True
            )
            mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=plt.get_cmap("hsv"))

        for ix in range(len(self)):

            # Get starting point
            line = Line.from_slope(self[ix], self.normals[ix], k[ix] * scalar)

            if "color" not in kwargs:
                line.plot(axes, color=mapper.to_rgba(k[ix]))
            else:
                line.plot(axes, **kwargs)

        return axes

    def draw_normal_lines(self, lengths=List[float], axes=None, **kwargs):
        """Generate normal lines along the curve

        Args:
            lengths (List[float] or number): length of lines
            axes (matplotlib.axes.Axes): axes for plot
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        if axes is None:
            axes = plt.gca()

        if isinstance(lengths, numbers.Real):
            lengths = [lengths for _ in range(len(self))]

        if "color" not in kwargs:
            norm = matplotlib.colors.Normalize(vmin=0, vmax=len(self), clip=True)
            mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=plt.get_cmap("hsv"))

        for ix in range(len(self)):

            # Get starting point
            line = Line.from_slope(self[ix], self.normals[ix], lengths[ix])

            if "color" not in kwargs:
                line.plot(axes, color=mapper.to_rgba(ix))
            else:
                line.plot(axes, **kwargs)

        return axes

    def draw_tangent_lines(self, lengths: List[float], axes=None, **kwargs):
        """Generate tangent lines along the curve

        Args:
            lengths (List[float] or number): length of lines
            axes (matplotlib.axes.Axes): axes for plot
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        if axes is None:
            axes = plt.gca()

        if isinstance(lengths, numbers.Real):
            lengths = [lengths for _ in range(len(self))]

        if "color" not in kwargs:
            norm = matplotlib.colors.Normalize(vmin=0, vmax=len(self), clip=True)
            mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=plt.get_cmap("hsv"))

        for ix in range(len(self)):

            # Get starting point
            line = Line.from_slope(self[ix], self.tangents[ix], lengths[ix])

            if "color" not in kwargs:
                line.plot(axes, color=mapper.to_rgba(ix))
            else:
                line.plot(axes, **kwargs)

        return axes

        """ Methods: 2D """

    """ Curvature """

    def curvature(self, method: str = "spline_t", offset: int = 2, show: bool = False):
        """Compute the curvature along the curve with a method of choice

        Method: 'spline t'
            Fit a spline to the entire curve, and use the analytical formula
            in the parametric space

        Method: 'analytical'
            Fit a known function to curve or segments of that curve,
            and use analytical formula with derviatives

        Alternative Methods: 'circle'
            Fit a circle to points and estimate the curvature using the
            reciprocal.

            Method will depend on the circular fit and number of points
            selected for the fit.

        Args:
            method (str): analytical approach for estimating curvature
            offset (int): number of points to gather on each side
            show (bool): plot the calculated distribution
        Returns:
            List[floats]: curvature values along curve
        References:
            http://tutorial.math.lamar.edu/Classes/CalcIII/Curvature.aspx
        """

        valid = ["spline_t", "spline_t_pw", "polynomial"]

        if method not in valid:
            raise ValueError(
                f"Invalid method: '{method}'. Valid methods include ({', '.join(valid)})"
            )

        if method == "spline_t":
            k = self.__curvature_analytical_spline_t()
        elif method == "spline_t_pw":
            k = self.__curvature_analytical_spline_t_pw(offset)
        elif method == "polynomial":
            k = self.__curvature_analytical_polynomial(offset)

        if show:
            range_i = list(range(len(self)))

            fig, axes = plt.subplots(2, 1)

            # Plot 1: airfoil
            axes[0] = self.plot(axes[0], color="black", label="Curve")
            plot_extras(
                axes[0],
                xlabel="Dimension i",
                ylabel="Dimension j",
                equal=True,
                show=False,
            )

            # Plot 2: angular distribution
            axes[1].plot(range_i, k, label="Curvature")
            plot_extras(axes[1], xlabel="Index, ix [-]", ylabel="Curvature, k [-]")

        return k

    def __curvature_analytical_spline_t(self):
        """Calculate the curvature using an analytical fit vs t

        Returns:
            List[numbers]: curvature between points
        References:
            http://www-users.math.umn.edu/~mille003/1372curvature.pdf
            https://www.solitaryroad.com/c361.html
        """
        # TODO: utilize precalculated tck values from parent Curve

        # Calculate normalized distance
        arr_s = self.get_normalized_s_distance()

        # Fit curve to points
        tck_j = interpolate.splrep(arr_s, self.j, s=0)
        dj1 = interpolate.splev(arr_s, tck_j, der=1)
        dj2 = interpolate.splev(arr_s, tck_j, der=2)

        # Fit curve to points
        tck_i = interpolate.splrep(arr_s, self.i, s=0)
        di1 = interpolate.splev(arr_s, tck_i, der=1)
        di2 = interpolate.splev(arr_s, tck_i, der=2)

        return np.abs(di1 * dj2 - di2 * dj1) / (di1 ** 2 + dj1 ** 2) ** 1.5

    def __curvature_analytical_spline_t_pw(self, offset: int = 2):
        """Calculate the curvature using an spline analytical fit
        over pieces of the curve

        Args:
            offset (int): number of points to gather on each side
        Returns:
            List[numbers]: curvature between points
        References:
            https://www.math24.net/curvature-radius/
            http://tutorial.math.lamar.edu/Classes/CalcIII/Curvature.aspx
        """

        k = [None for ix in range(len(self))]

        arr_s = self.get_normalized_s_distance()

        for ix in range(offset, len(self) - offset):

            # Fit curve to slice of points
            tck_j = interpolate.splrep(
                arr_s[ix - offset : ix + offset + 1],
                self.j[ix - offset : ix + offset + 1],
                s=0,
            )
            dj1 = interpolate.splev(arr_s[ix], tck_j, der=1)
            dj2 = interpolate.splev(arr_s[ix], tck_j, der=2)

            # Fit curve to slice of points
            tck_i = interpolate.splrep(
                arr_s[ix - offset : ix + offset + 1],
                self.i[ix - offset : ix + offset + 1],
                s=0,
            )
            di1 = interpolate.splev(arr_s[ix], tck_i, der=1)
            di2 = interpolate.splev(arr_s[ix], tck_i, der=2)

            # Evaluate at point
            k[ix] = np.abs(di1 * dj2 - di2 * dj1) / (di1 ** 2 + dj1 ** 2) ** 1.5

        return k

    def __curvature_analytical_polynomial(self, offset: int = 2):
        """Calculate the curvature using a polynomial analytical fit

        Args:
            offset (int): number of points to gather on each side
        Returns:
            List[numbers]: curvature between points
        References:
            https://www.math24.net/curvature-radius/
            http://tutorial.math.lamar.edu/Classes/CalcIII/Curvature.aspx
        """

        k = [None for ix in range(len(self))]

        for ix in range(offset, len(self) - offset):

            arr_i = np.array(self.i[ix - offset : ix + offset + 1])
            arr_j = np.array(self.j[ix - offset : ix + offset + 1])

            # Fit polynomial
            z = np.polyfit(arr_i, arr_j, deg=2)
            p = np.poly1d(z)

            # Calculate dervatives
            d1 = np.polyder(p, m=1)
            d2 = np.polyder(p, m=2)

            # Fit function to curve
            evl_d1 = d1(self.i[ix])
            evl_d2 = d2(self.i[ix])

            k[ix] = np.abs(evl_d2) / (1 + evl_d1 ** 2) ** 1.5

        return k

    """ Curvature: evolute """
    # TODO: calculate evolute and curvature simultaneously
    def evolute(self):
        """Compute the center of curvature

        Returns:
            Tuple(List[float], List[float]): coordinates of evolute

        References:
            https://en.wikipedia.org/wiki/Evolute
        """

        # Calculate normalized distance
        arr_s = self.get_normalized_s_distance()

        # Fit curve to points
        tck_j = interpolate.splrep(arr_s, self.j, s=0)
        dj0 = interpolate.splev(arr_s, tck_j, der=0)
        dj1 = interpolate.splev(arr_s, tck_j, der=1)
        dj2 = interpolate.splev(arr_s, tck_j, der=2)

        # Fit curve to points
        tck_i = interpolate.splrep(arr_s, self.i, s=0)
        di0 = interpolate.splev(arr_s, tck_i, der=0)
        di1 = interpolate.splev(arr_s, tck_i, der=1)
        di2 = interpolate.splev(arr_s, tck_i, der=2)

        x = di0 - (dj1 * (di1 ** 2 + dj1 ** 2)) / (di1 * dj2 - di2 * dj1)
        y = dj0 + (di1 * (di1 ** 2 + dj1 ** 2)) / (di1 * dj2 - di2 * dj1)

        return x, y

    def draw_evolute(self, axes=None, connect: bool = True, **kwargs):
        """Draw the center of curvatures aka the evolute

        Args:
            axes (matplotlib.axes.Axes): axes for plot
            connect (bool): connect point with its center of curvature
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        evl_x, evl_y = self.evolute()

        if axes is None:
            axes = plt.gca()

        if "color" not in kwargs:
            norm = matplotlib.colors.Normalize(vmin=0, vmax=len(self), clip=True)
            mapper = matplotlib.cm.ScalarMappable(norm=norm, cmap=plt.get_cmap("hsv"))

        if connect:

            for ix in range(len(self)):
                if "color" not in kwargs:
                    axes.plot(
                        [self[ix].i, evl_x[ix]],
                        [self[ix].j, evl_y[ix]],
                        linestyle="-",
                        color=mapper.to_rgba(ix),
                        **kwargs,
                    )
                else:
                    axes.plot(
                        [self[ix].i, evl_x[ix]],
                        [self[ix].j, evl_y[ix]],
                        linestyle="-",
                        **kwargs,
                    )
        else:
            axes.plot(evl_x, evl_y, **kwargs)

        return axes


class Line(Curve):
    """Curve with 2 points"""

    def __init__(self, points: List[Point]):

        if len(points) != 2:
            raise ValueError(f"Lines must contain 2 points instead of {len(points)}")

        super().__init__(points)

    @classmethod
    def from_slope(cls, start: Point, slopes: np.array, length: float = 1.0):
        """Create a curve from a text file

        Args:
            start (Point): starting point
            slopes (np.array): slopes of line
            length (float): length of line
        Returns:
            Line: array of points in 3d space
        """

        # Compute new ijk
        t = cls.get_t_by_slopes_and_length(slopes, length)
        stop = np.array(start.coords) + slopes * t

        return cls([start, Point(*stop)])

    @classmethod
    def from_start_stop(cls, start: Point, stop: Point):
        """Create a curve from a starting and ending point

        Args:
            start (Point): starting point
            stop (Point): ending point
        Returns:
            Line: array of points in 3d space
        """
        return cls([start, stop])

    """ Properties: general """

    def __len__(self) -> int:
        """Line is defined by two points"""
        return 2

    def __repr__(self) -> str:
        """Object representation: must overwrite"""
        return f"Line({self.start} -> {self.stop})"

    """ Properties """

    @property
    def intercepts(self) -> np.array:
        """Get the slopes for the current line

        r = r0 + t*v

        Returns:
            np.array: intercepts in ijk directions
        """
        return np.array(self.start.coords)

    @property
    def length(self) -> float:
        """Distance from starting point to ending point"""
        return self.start.dist(self.stop)

    @property
    def midpoint(self) -> Point:
        """Compute mid point"""

        # Average points
        return (self.start + self.stop) * Point(0.50, 0.50, 0.50)

    @property
    def slopes(self) -> np.array:
        """Get the slopes for the current line

        r = r0 + t*v

        Returns:
            np.array: slopes in ijk directions
        """
        return self.vector

    @property
    def vector(self) -> np.array:
        """Compute vector

        v = P(B) - P(A)
        """
        return np.array(self.stop.coords) - np.array(self.start.coords)

    @property
    def unit_vector(self) -> np.array:
        """ Compute unit vector """

        # Grab vector
        vec = self.vector

        # Normalize by norm
        norm = np.sqrt(np.sum(vec ** 2))
        return vec / norm

    """ Methods: static """

    @staticmethod
    def get_t_by_slopes_and_length(slopes: np.array, length: float = 1.0):
        """Compute the parametric value necessary for a given length

        (r - r0)^2 = length^2

        where position: r  = <i,  j,  k>
        where origin  : r0 = <i0, j0, k0>

        Args:
            slopes (np.array): slopes in each dimension
            length (float): distance of new point from origin
        Returns:
            float: parametric value t
        """
        mag = np.sum(np.square(slopes))
        t = np.sqrt(length ** 2 / mag)

        # Flip signs for direction
        if length > 0:
            return t
        else:
            return -t

    """ Methods: instance """

    def get_ijk(self, t: float) -> np.array:
        """Compute ijk location for the given parametric value

        r = r0 + v * t

        where position: r  = <i,  j,  k>
        where origin  : r0 = <i0, j0, k0>
        where slopes  :  m = <mi, mj, mk>

        Args:
            t (float): parametric location
        Returns:
            np.array: new ijk locations
        References:
            http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfLines.aspx
        """

        # Evauate ijk at new t
        return self.intercepts + self.vector * t

    def get_t_by_length(self, length: float = 1.0):
        """Compute the parametric value necessary for a given length

        (r - r0)^2 = length^2

        where position: r  = <i,  j,  k>
        where origin  : r0 = <i0, j0, k0>

        Args:
            length (self): distance of new point from origin
        Returns:
            float: parametric value t
        """

        return self.get_t_by_slopes_and_length(self.vector, length)

    def intersection(self, other):
        """Intersection with another object: must overwrite"""
        raise NotImplementedError()


class Line2D(Line):
    """Curve with 2 points in ij space"""

    def __init__(self, points: List[Point]):
        super().__init__(points)

    """ Properties: general """

    def __repr__(self) -> str:
        """Object representation"""
        return f"Line2D({self.start} -> {self.stop})"

    """ Properties """

    @property
    def slope(self) -> float:
        """Compute slope of the line

        y = m*x + b

        m = (y2 - y1) / (x2 - x1)

        Returns:
            float: line slope
        """
        return self.start.slope_ij(self.stop)

    @property
    def intercept(self) -> float:
        """Compute intercept of the line

        Equation of a line
            y = mx + b

        Rearrange to solve for intercept
            b = y - mx

        Plug in known values of x and y
            b = y0 - m*x0

        Returns:
            float: intercept of line on y axis
        """
        # TODO: verify intercept is correct
        return self.start.j - self.slope * self.start.i

    @property
    def normal(self) -> float:
        """Compute the normal of the current line

        The normal is perpendicular to the current line

        normal = -dx /dy

        References:
            https://en.wikipedia.org/wiki/Normal_(geometry)
            https://mathinsight.org/tangent_normal_lines_refresher
        """
        return -1.0 / self.slope

    """ Methods """

    def normal_line(self, length: float = 1.0):
        """Compute a line normal to the midpoint

        Args:
            length (float): length of the normal line
        Returns:
            Line: line normal to the midpoint
        """
        # Calculate intercept for normal (y = mx + b)
        # b = y0 - m * x0
        b = self.midpoint.j - self.normal * self.midpoint.i

        # Angle: tan(theta) = dy / dx
        theta = np.arctan(self.normal)

        # We want to find length such
        # dx = r * cos(theta)
        # x' = x + dx
        x = self.midpoint.i + length * np.cos(theta)

        # Evaluate line at new point
        y = self.normal * x + b

        return Line2D([self.midpoint, Point(x, y)])

    """ Methods: Intersection """

    @staticmethod
    def compute_h(line1: Line, line2: Line) -> float:
        """Compute h

        Args:
            line1 (Line): line from Point(A) to Point(B)
            line2 (Line): line from Point(C) to Point(D)
        Returns:
            float: h value
        References:
            https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
        """

        # Compute E
        #  E = B-A = (Bx-Ax, By-Ay)
        e = line1.stop - line1.start

        # F = D-C = (Dx-Cx, Dy-Cy)
        f = line2.stop - line2.start

        # P = (-Ey, Ex)
        p = Point(-e.j, e.i)

        # h = ( (A-C) * P ) / ( F * P )
        intersection = f.i * p.i + f.j * p.j

        # Parallel lines
        if intersection == 0:
            return None
        else:
            return (
                (line1.start.i - line2.start.i) * p.i
                + (line1.start.j - line2.start.j) * p.j
            ) / intersection

    def intersection(self, other):
        """Determine intersection between two 2D lines

        Args:
            other: secondary object for consideration
        Returns:
            Tuple[bool, Point]: if intersection occurs and location of intersection
        References:
            https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
            https://jsfiddle.net/ferrybig/eokwL9mp/
        """
        # TODO: extend method to handle 2d coordinates other than ij

        # Specify tolerance (must be positive integer)
        tol = 1e-5

        if isinstance(tol, numbers.Number) is False:
            raise TypeError(f"Expecting a numerical value instead of {type(tol)}")
        if tol < 0:
            raise ValueError("Tolerance must be a positive value")

        if isinstance(other, Line):

            # Compute h values
            h1 = self.compute_h(self, other)
            h2 = self.compute_h(other, self)
            is_parallel: bool = h1 is None or h2 is None

            if is_parallel:
                return False, None

            # Values are currently intersecting
            intersects: bool = (0 <= h1 <= 1) and (0 <= h2 <= 1)

            # Compute intersection point
            # F = (D.x - C.x, D.y - C.y)
            f = other.stop - other.start

            #  C + F*h
            return intersects, Point(other.start.i + f.i * h1, other.start.j + f.j * h1)

        elif isinstance(other, Point):

            # Evaluate the point (y = mx + b)
            y = self.slope * other.i + self.intercept

            # Compare evaluation to line
            return abs(other.j - y) <= tol

        else:
            raise TypeError(f"Expecting Point or Line object instead of {type(other)}")


class Line3D(Line):
    """Curve with 2 points in ijk space"""

    def __init__(self, points: List[Point]):
        super().__init__(points)

    """ Properties: general """

    def __repr__(self) -> str:
        """Object representation"""
        return f"Line3D({self.start} -> {self.stop})"

    """ Intersection """

    def intersection(self, other, tol: numbers.Real = 1e-5):
        """Compute intersection between the line and another object

        Args:
            other (Line/Point):
            tol (numbers.Real): allowable tolerance for intersection
        Returns:
            Point: location of intersection
        References:
            https://en.wikipedia.org/wiki/Line–line_intersection
            https://math.stackexchange.com/questions/28503/how-to-find-intersection-of-two-lines-in-3d
            https://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines

            http://paulbourke.net/geometry/pointlineplane/
        """

        if isinstance(tol, numbers.Real) is False:
            raise TypeError(f"Expecting a numerical value instead of {type(tol)}")
        elif tol < 0:
            raise ValueError("Tolerance must be a positive value")

        if isinstance(other, Line):
            raise NotImplementedError()
        elif isinstance(other, Point):
            raise NotImplementedError()
        else:
            raise TypeError(f"Expecting Line or Point type instead of {type(other)}")


class Airfoil(object):
    """Airfoil made of two 2D curves"""

    def __init__(self, upper: Curve, lower: Curve):
        """Create an airfoi using two curves

        Args:
            upper (Curve): top airfoil curve
            lower (Curve): bottom airfoil curve
        Notes:
            This method assumes that the upper surface is ordered from
            LE to TE, and the lower surface is ordered from TE to LE.
            The two surfaces combined should essentially create a CCW loop.

            It also assumes that the curves are essentially 2D.
            ie: constant radii, constant normalized span
        """

        print("Creating airfoil")

        # todo: combine points from le to te, then te to le
        # todo: ensure that the point distance isn't too large
        # todo: compute camber line
        # TODO: extrapolate camber line for curve splitting
        # TODO: split curves by camberline, and update te, le
        # todo: estimate leading edge radii (based upon points)
        # todo: estimate trailing edge radii
        # TODO: remove duplicate points

        # TODO: iterate once and precompute min max, area, centroid

        # TODO: remove surface setter, and just create a new airfoil instead?
        self.upper = upper
        self.lower = lower

        # Check orientations
        self.check_orientation_vertical()
        self.check_orientation_horizontal()
        self.check_orientation_rotation()

        # Check for duplicates
        # self.remove_duplicates()

        # Concatenate curves (exclude duplicate points)
        self.__points = Curve(self.upper.points[:-1] + self.lower.points)

        # Empty inits
        self.__area: float = None
        self.__centroid: Point = None

        # Camber variables
        self.camber_line: Curve = None
        self.camber_radii: List[float] = None

    """ Properties: Curves """

    @property
    def upper(self) -> Curve:
        """Get the upper side"""
        return self.__upper

    @property
    def lower(self) -> Curve:
        """Get the lower side"""
        return self.__lower

    @upper.setter
    def upper(self, curve: Curve):
        """Set the upper side"""

        if isinstance(curve, Curve) is False:
            raise TypeError(
                f"Input curve must be of Curve type instead of {type(curve)}"
            )

        self.__upper = curve

    @lower.setter
    def lower(self, curve: Curve):
        """Get the lower side"""

        if isinstance(curve, Curve) is False:
            raise TypeError(
                f"Input curve must be of Curve type instead of {type(curve)}"
            )

        self.__lower = curve

    """ Properties: Points """

    @property
    def le(self) -> Point:
        """Grab leading edge point: based upon minimum i"""

        if self.lower.stop.i <= self.upper.start.i:
            return self.lower.stop
        else:
            return self.upper.start

    @property
    def te(self) -> Point:
        """Grab trailing edge point: based upon maximum i"""

        if self.lower.start.i >= self.upper.stop.i:
            return self.lower.start
        else:
            return self.upper.stop

    """ Check Orientation """

    def check_orientation_rotation(self):
        """Check if the airfoil needs to be rotated"""

        print(" Checking orientation: rotational")

        min_i = float("inf")
        max_i = float("-inf")
        min_j = float("inf")
        max_j = float("-inf")

        # Grab bounding box
        for pt in self.upper:
            min_i = min(pt.i, min_i)
            max_i = max(pt.i, max_i)

            min_j = min(pt.j, min_j)
            max_j = max(pt.j, max_j)

        for pt in self.lower:
            min_i = min(pt.i, min_i)
            max_i = max(pt.i, max_i)

            min_j = min(pt.j, min_j)
            max_j = max(pt.j, max_j)

        # Compute the 'aspect ratio' of the airfoil section
        length = max_i - min_i
        height = max_j - min_j

        ratio = height / length
        print(f"  {'length':<12}: {length:.3f}")
        print(f"  {'height':<12}: {height:.3f}")
        print(f"  {'aspect ratio':<12}: {ratio:.3f}")

        # Airfoils should be larger in length direction
        if ratio > 1:
            print("This airfoil may be oriented incorrectly")

            # Plot and add extras
            axes = self.plot_curves()
            plot_extras(axes, equal=True, show=True)

    def check_orientation_horizontal(self):
        """Check the horizontal orientation between both sides

        Airfoil points should essentially meet. The
        upper surface should end where the lower surface begins
        """
        print(" Checking orientation: horizontal")

        # Compute distance from end to beggining
        dist_now = self.upper.stop.dist(self.lower.start)
        dist_flip = self.upper.stop.dist(self.lower.stop)

        # Compare distances
        if dist_flip < dist_now:
            print("  Curves appear to have different horizontal orientations")

            # Plot airfoil
            axes = self.plot_curves(annotate=True)
            axes = self.plot_start_stop(axes)
            plot_extras(axes, equal=True, show=True)

            # Prompt user for action
            value = get_user_input(
                "  Would you like to reverse a curve? [y/n]: ", set(["y", "n"])
            )

            # Flip orientation
            if value == "y":

                # Find 'correct' orientation
                # Upper looks correct, reverse lower
                if self.upper.start.i < self.lower.start.i:
                    self.lower.reverse()
                else:
                    self.upper.reverse()

    def check_orientation_vertical(self):
        """Check the vertical orientation between both sides

        Compare the average j coordinate between both surfaces to determin
        if the upper surface is actually the 'top' surface.

        This method assumes a similar point density amongst both curves.
        If the curves appear to be incorrectly labeled, we swap them
        """

        print(" Checking orientation: vertical")

        # Compute average coordinate
        upper_avg = np.mean(self.upper.j)
        lower_avg = np.mean(self.lower.j)

        if lower_avg > upper_avg:

            print("  Lower curve appears to be above the upper curve")

            # Plot airfoil
            axes = self.plot_curves()
            self.plot_start_stop(axes)
            plot_extras(axes, equal=True, show=True)

            # Prompt user for action
            value = get_user_input(
                "  Would you like to swap curves? [y/n]: ", set(["y", "n"])
            )

            # Flip orientation
            if value == "y":

                # Swap curves
                self.upper, self.lower = self.lower, self.upper

    """ Airfoil Parameters """

    def __compute_area_centroid(self):
        """Compute the area and centroid at the same time

        References:
            http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/
        """

        area: float = 0.0
        cx: float = 0.0
        cy: float = 0.0

        for ix in range(len(self.__points) - 1):

            # Compute common terms
            xi_yip1 = self.__points[ix].i * self.__points[ix + 1].j
            xip1_yi = self.__points[ix + 1].i * self.__points[ix].j
            term1 = xi_yip1 - xip1_yi

            # Add current contribution
            area += term1
            cx += (self.__points[ix].i + self.__points[ix + 1].i) * term1
            cy += (self.__points[ix].j + self.__points[ix + 1].j) * term1

        # Save
        self.__area = area * 0.50
        self.__centroid = Point(cx / (6.0 * area), cy / (6.0 * area))

    def area(self) -> float:
        """Compute the area of an airfoil assuming fully solid

        Returns:
            float: solid area
        References:
            http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/
            """
        # TODO: reset on curve update
        # TODO: move this to Curve or a subset or 'ClosedCurve'

        if self.__area is None:
            self.__compute_area_centroid()
        return self.__area

    def camberline(self):
        """Compute camberline between surfaces"""
        # TODO: need to account for when curve(s) are updated
        raise NotImplementedError()

    def centroid(self) -> Point:
        """Compute centroid for airfoil

        Returns:
            Point: center of area
        References:
            http://local.wasp.uwa.edu.au/~pbourke/geometry/polyarea/
        """
        # TODO: need to account for when curve(s) are updated
        if self.__centroid is None:
            self.__compute_area_centroid()
        return self.__centroid

    def chord(self) -> float:
        """Compute average chord using the start and stop points

        Returns:
            float: average chord
        """

        # Compute chord and average
        chord_upper = self.upper.start.dist(self.upper.stop)
        chord_lower = self.lower.start.dist(self.lower.stop)
        return (chord_upper + chord_lower) * 0.5

    def thickness(self, show: bool = True):
        """Compute thickness distrbution between curves

        Assuming a two dimensional curve

        Args:
            show (bool): show a plot of the distribution
        Returns:
            List[floats]: thickness distribution
        """

        # Grab interpolation range
        i_min = min(min(self.upper.i), min(self.lower.i))
        i_max = max(max(self.upper.i), max(self.lower.i))
        range_i = np.linspace(i_min, i_max, 100)

        # Fit surfaces
        tck_u = interpolate.splrep(self.upper.i, self.upper.j, s=0)
        tck_l = interpolate.splrep(self.lower.i[::-1], self.lower.j[::-1], s=0)

        # Apply fit
        upper_intp = interpolate.splev(range_i, tck_u, der=0)
        lower_intp = interpolate.splev(range_i, tck_l, der=0)

        # Delta
        thickness = upper_intp - lower_intp  # distribution of thickenss List[float]
        max_thick = range_i[np.argmax(thickness)]  # location of max thickness [float]

        if show:
            # Create plot of curve, then thickness
            fig, axes = plt.subplots(2, 1)

            # Plot 1: airfoil
            axes[0] = self.plot_curves(axes[0], annotate=False)
            plot_extras(
                axes[0],
                xlabel="Dimension i",
                ylabel="Dimension j",
                equal=True,
                show=False,
            )

            # Plot 2: thickness
            axes[1].plot(range_i, thickness)
            axes[1].axvline(max_thick, color="red", label="Max Thickness")
            plot_extras(axes[1], xlabel="Dimension i", ylabel="Thickness")

        return thickness

    """ Transformations """

    def __general_transform(self, method: str, *args, **kwargs):
        """Apply arbitrary method to airfoil curves

        Args:
            method (str): function to apply to objects
        Returns:
            None
        """
        lower = getattr(self.lower, method)(*args, **kwargs)
        upper = getattr(self.upper, method)(*args, **kwargs)

        return Airfoil(upper, lower)

    def rotate_i(self, *args, **kwargs):
        """Rotate curves about the i axis

        Returns:
            Airfoil: modified airfoil curve
        """
        return self.__general_transform("rotate_i", *args, **kwargs)

    def rotate_j(self, *args, **kwargs):
        """Rotate curves about the j axis

        Returns:
            Airfoil: modified airfoil curve
        """
        return self.__general_transform("rotate_j", *args, **kwargs)

    def rotate_k(self, *args, **kwargs):
        """Rotate curves about the k axis

        Returns:
            Airfoil: modified airfoil curve
        """
        return self.__general_transform("rotate_k", *args, **kwargs)

    def scale(self, *args, **kwargs):
        """Scale curves about the central point

        Returns:
            Airfoil: modified airfoil curve
        """
        return self.__general_transform("scale", *args, **kwargs)

    def translate(self, *args, **kwargs):
        """Translate curves in ijk directions

        Returns:
            Airfoil: modified airfoil curve
        """
        return self.__general_transform("translate", *args, **kwargs)

    """ Plots """

    def plot_camberline(
        self, axes=None, camberline: Curve = None, radii: List[float] = None
    ):
        """Plot the current airfoil sections

        Args:
            axes (matplotlib.axes): axis to plot on
        Returns:
            matplotlib.axes.Axes: axes with plotted curves
        """
        # Define plot
        if axes is None:
            axes = plt.gca()

        if camberline is None:
            self.camber_line, self.camber_radii = self.camber_line()

            camberline = self.camber_line

        # Plot camber line
        axes.plot(
            camberline.i,
            camberline.j,
            label="Camberline",
            color="cyan",
            marker="o",
            linestyle="--",
        )

        # Plot circles along camber line
        if radii:
            for ix in range(len(camberline)):
                circle = plt.Circle(
                    (camberline[ix].i, camberline[ix].j), radius=radii[ix], fill=False
                )
                axes.add_patch(circle)

        return axes

    def plot_curve(self, axes=None, **kwargs):
        """Plot the current airfoil as a single curve

        Args:
            axes (matplotlib.axes): axis to plot on
        Returns:
            matplotlib.axes.Axes: axes with plotted curve
        """

        # Combine key words
        kwargs_raw = {
            "marker": "o",
            "linestyle": "-",
            "markersize": 5,
            "color": "black",
        }
        kwargs_com = {**kwargs_raw, **kwargs}

        # Define plot
        if axes is None:
            axes = plt.gca()

        # Plot curves
        axes.plot(self.upper.i, self.upper.j, **kwargs_com)

        # Remove label to appear as the same curve
        kwargs_com["label"] = None
        axes.plot(self.lower.i, self.lower.j, **kwargs_com)

        return axes

    def plot_curves(self, axes=None, annotate: bool = False, **kwargs):
        """Plot the current airfoil sections seperately

        Args:
            axes (matplotlib.axes): axis to plot on
            annotate (bool): add index labels to the points
        Returns:
            matplotlib.axes.Axes: axes with plotted curves
        """

        kwargs_raw = {"marker": "o", "linestyle": "-", "markersize": 5}
        kwargs_com = {**kwargs_raw, **kwargs}

        if "label" in kwargs:
            label_u = kwargs["label"] + ": Upper"
            label_l = kwargs["label"] + ": Lower"
        else:
            label_u = "Upper"
            label_l = "Lower"

        # Define plot
        if axes is None:
            axes = plt.gca()

        # Plot curves
        axes.plot(
            self.upper.i, self.upper.j, label=label_u, color="dodgerblue", **kwargs_com
        )
        axes.plot(
            self.lower.i,
            self.lower.j,
            label=label_l,
            color="mediumspringgreen",
            **kwargs_com,
        )

        if annotate:
            kwargs_ant = {"fontsize": 13}

            for ix in range(len(self.upper)):
                axes.annotate(
                    ix,
                    (self.upper.i[ix], self.upper.j[ix]),
                    color="dodgerblue",
                    **kwargs_ant,
                )

            for ix in range(len(self.lower)):
                axes.annotate(
                    ix,
                    (self.lower.i[ix], self.lower.j[ix]),
                    color="mediumspringgreen",
                    **kwargs_ant,
                )

        return axes

    def plot_start_stop(self, axes=None):
        """Plot the start and stop points

        Args:
            axes (matplotlib.axes): axis to plot on
        Returns:
            matplotlib.axes.Axes: axes with plotted curves
        """

        kwargs = {"marker": "o", "linestyle": "None", "markersize": 7}

        # Define plot
        if axes is None:
            axes = plt.gca()

        # Add emphasis for starting and ending points
        axes.plot(
            [self.upper.start.i],
            [self.upper.start.j],
            color="red",
            label="Upper: Start",
            **kwargs,
        )
        axes.plot(
            [self.upper.stop.i],
            [self.upper.stop.j],
            color="purple",
            label="Upper: Stop",
            **kwargs,
        )

        return axes

    def plot_curvature(
        self,
        axes=None,
        scale: float = 1.0,
        color_u: str = "mediumspringgreen",
        color_l: str = "deeppink",
        **kwargs,
    ):
        """Plot normal lines of curvature

        Args:
            axes (matplotlib.axes): axis to plot on
            scale (float): multiplier on curvature values
            scale (float): multiplier on curvature values
            color_u (str): color for upper curvature lines
            color_l (str): color for lower curvature lines
        Returns:
            matplotlib.axes.Axes: axes with plotted curves
        """
        # Define plot
        if axes is None:
            axes = plt.gca()

        if "label" in kwargs:
            label_u = kwargs["label"] + ": Upper"
            label_l = kwargs["label"] + ": Lower"
            del kwargs["label"]
        else:
            label_u = None
            label_l = None

        # Add legend labels
        axes.plot([], [], label=label_u, color=color_u, **kwargs)
        axes.plot([], [], label=label_l, color=color_l, **kwargs)

        # Plot curves
        self.upper.draw_curvature_normal_lines(scale, axes, color=color_u, **kwargs)
        self.lower.draw_curvature_normal_lines(scale, axes, color=color_l, **kwargs)

        return axes


# TODO: any more efficient methods for subject tracking (Curve, Curve2D)
