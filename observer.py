from abc import ABCMeta, abstractmethod


class Observer(metaclass=ABCMeta):
    """Get an update from subject"""

    @abstractmethod
    def update(self, subject):
        """Get an update from subject"""
        raise NotImplementedError()


class Subject(metaclass=ABCMeta):
    """Interface for managing subscribers
    
    References:
        https://refactoring.guru/design-patterns/observer
    """

    @abstractmethod
    def attach(self, observer: Observer):
        """Attach an observer to the current subject"""
        raise NotImplementedError()

    @abstractmethod
    def detach(self, observer: Observer):
        """Detach an observer from the current subject"""
        raise NotImplementedError()

    @abstractmethod
    def notify(self):
        """Notify the subject about an event"""
        raise NotImplementedError()
