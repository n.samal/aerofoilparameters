from matplotlib import pyplot as plt
import numpy as np
import os
from scipy import interpolate
import sys
from typing import Dict, List

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from utilities import plot_extras, get_colors
from geometry import Point, Curve, Curve2D, Airfoil

# Dimensions
X = 'x'
Y = 'y'

# Data labels
DATA_RAW = 'Raw Data'
INTP_RAW = 'Interpolated Data'

# Airfoil nomenclature: Surfaces
SIDE_PS = 'Pressure Side'
SIDE_SS = 'Suction Side'

SIDE_LW = 'Lower'
SIDE_UP = 'Upper'

# Airfoil nomenclature: Points
AX_MIN = 'Axial Min'
AX_MAX = 'Axial Max'

LE = 'Leading Edge'
TE = 'Trailing Edge'


def reorder_curve(curve):

    x = np.array(curve.i)

    # Get min and max indices
    ix_min = np.argmin(x)
    ix_max = np.argmax(x)

    # Swap if max is smaller
    if ix_max < ix_min:
        ix_min, ix_max = ix_max, ix_min

    print(f'  ix_min: {ix_min}')
    print(f'  ix_max: {ix_max}')

    return curve


def calculate_camberline(curve1, curve2, show: bool=False):
    """Calculate the mean camberline

    Args:
        curve1 (Curve): top curve
        curve2 (Curve): bottom curve
    Returns:
        Curve: mean camberline
    """
    camber_line = []
    radii = []
    prior_jx: int = 1

    # Plot args
    kwargs_raw = {'marker': 'o', 'linestyle': 'None', 'markersize': 5}
    kwargs_nrm = {
        'marker': 'o',
        'markersize': 5,
        'linestyle': '-',
        'linewidth': 3
    }

    tol: float = 1e-5

    # Go through all points
    for ix in range(1, len(curve1)):

        print(f' ix:{ix}')

        # Grab line
        line1 = curve1[ix - 1:ix + 1]

        # Compute midpoint and normal
        line1_mid = line1.midpoint
        line1_normal = line1.normal_line()

        # Compute dot product between lines
        dot1 = np.dot(line1.vector, line1_normal.vector)

        if dot1 > tol:
            raise ArithmeticError(f'Non zero dot product: {dot1}')

        # Iterate through second curve
        for jx in range(prior_jx, len(curve2)):

            print(f' jx:{jx}')

            # Get line segment
            line2 = curve2[jx - 1:jx + 1]

            # Compute normal line and intersection
            line2_mid = line2.midpoint
            line2_normal = line2.normal_line()
            intersects, int_pt = line2_normal.intersection(line1_normal)

            # Compute dot product between lines
            dot2 = np.dot(line2.vector, line2_normal.vector)

            if dot2 > tol:
                raise ArithmeticError(f'Non zero dot product: {dot2}')

            # Mean point
            # radii_1 ~= radii_2
            # intersection point y between both mid points
            # value less than fraction of chord

            # Min and max
            min_j = min([line1_mid.j, line2_mid.j])
            max_j = max([line1_mid.j, line2_mid.j])

            if int_pt and min_j <= int_pt.j <= max_j and abs(
                    int_pt.dist(line1_mid) - int_pt.dist(line2_mid)) <= 1e-3:

                # Save camberline and radii
                camber_line.append(int_pt)
                radii.append(int_pt.dist(line1_mid))
                prior_jx = jx  # save indice found

                if show:

                    axes = plt.gca()

                    # axes.plot(x, y, label=DATA_RAW, marker='o', color='black')
                    axes.plot(curve1.i,
                              curve1.j,
                              label='Side 1',
                              color='red',
                              **kwargs_raw)
                    axes.plot(curve2.i,
                              curve2.j,
                              label='Side 2',
                              color='orange',
                              **kwargs_raw)

                    # Plot line and point point
                    axes.plot(line1.i, line1.j, color='purple', marker=None)
                    axes.plot(line1_mid.i,
                              line1_mid.j,
                              color='pink',
                              marker='o',
                              ms=5)

                    axes.plot(line1.i, line1.j, color='purple', marker=None)
                    axes.plot(line2_mid.i,
                              line2_mid.j,
                              color='pink',
                              marker='o',
                              ms=5)

                    # Plot first normal
                    axes.plot(line1_normal.i,
                              line1_normal.j,
                              color='lime',
                              label='Normal: line1',
                              **kwargs_nrm)

                    # Plot second normal
                    axes.plot(line2_normal.i,
                              line2_normal.j,
                              color='dodgerblue',
                              label='Normal: line2',
                              **kwargs_nrm)

                    # Plot intersection
                    axes.plot(int_pt.i,
                              int_pt.j,
                              marker='o',
                              color='pink',
                              ms=7)

                    # Add extras
                    plot_extras(axes, equal=True, show=True)

                continue
    # Camberline
    camber_line = Curve(camber_line)

    # Plot camberline
    axes = plt.gca()
    axes.plot(curve1.i, curve1.j, label='Side 1', color='red', **kwargs_raw)
    axes.plot(curve2.i, curve2.j, label='Side 2', color='orange', **kwargs_raw)

    # Add extras
    plot_extras(axes, equal=True, show=True)


def calculate_camberline2(curve1, curve2, show: bool=False):
    """Calculate the mean camberline

    Args:
        curve1 (Curve): top curve
        curve2 (Curve): bottom curve
    Returns:
        Curve: mean camberline
    """
    # Calculate estimates
    chord_est = curve2.start.dist(curve2.stop)

    camber_line = []
    radii = []
    prior_jx: int = 1

    # Plot args
    kwargs_raw = {'marker': 'o', 'linestyle': 'None', 'markersize': 5}
    kwargs_nrm = {
        'marker': 'o',
        'markersize': 5,
        'linestyle': '-',
        'linewidth': 3
    }

    # Go through all points
    for ix in range(1, len(curve1)):

        print(f' ix:{ix}')

        # Grab line
        line1 = curve1[ix - 1:ix + 1]

        # Compute midpoint and normal
        line1_mid = line1.midpoint
        line1_normal = line1.normal_line()

        # Reset min criteria
        min_pt: Point = None
        min_radii = float('inf')
        min_radii_ratio = float('inf')

        # Iterate through second curve
        for jx in range(1, len(curve2)):

            print(f' jx:{jx}')

            # Get line segment
            line2 = curve2[jx - 1:jx + 1]

            # Compute normal line and intersection
            line2_mid = line2.midpoint
            line2_normal = line2.normal_line()
            intersects, int_pt = line2_normal.intersection(line1_normal)

            # Mean point
            # radii_1 ~= radii_2
            # intersection point y between both mid points
            # value less than fraction of chord

            # Define y bounds
            min_j = min([line1_mid.j, line2_mid.j])
            max_j = max([line1_mid.j, line2_mid.j])

            # Compute radii
            radii1 = int_pt.dist(line1_mid)
            radii2 = int_pt.dist(line2_mid)
            radii_ratio = abs(radii1 / radii2 -  1.0)

            # We beat our previous best
            if int_pt and min_j <= int_pt.j <= max_j and radii_ratio <= min_radii_ratio:

                min_pt = int_pt
                min_radii = radii1
                min_radii_ratio = radii_ratio
                prior_jx = jx

            # Ratio is already good
            if radii_ratio <= 1e-3:
                continue

        if min_pt:
            # Save camberline and radii
            camber_line.append(min_pt)
            radii.append(min_radii)

    # Camberline
    camber_line = Curve(camber_line)

    # Plot camberline
    axes = plt.gca()

    axes.plot(curve1.i, curve1.j, label='Side 1', color='red', **kwargs_raw)
    axes.plot(curve2.i, curve2.j, label='Side 2', color='orange', **kwargs_raw)

    # Plot camber line
    axes.plot(camber_line.i,
              camber_line.j,
              label='Camberline',
              color='cyan',
              marker='o',
              linestyle='--')

    for ix in range(len(camber_line)):

        circle = plt.Circle((camber_line[ix].i, camber_line[ix].j), radius=radii[ix], fill=False)
        axes.add_patch(circle)

    # Add extras
    plot_extras(axes, equal=True, show=True)


def plot_interpolate_airfoil(curve1, curve2, show: bool=False):
    """Plot the airfoil curves

    Args:
        axes: 
        show (bool): plot the intersection of normals
    Returns:
        None
    """

    print(' plot_curves:')

    # curve2.reverse()

    # curve1 = curve[ix_min:ix_max + 1]
    # curve2 = curve[ix_max:] + curve[:ix_min]

    # Test interpolation
    curve1i = curve1.interpolate_by_s(200)
    curve2i = curve2.interpolate_by_s(200)

    # Plot camberline
    axes = plt.gca()

    kwargs_raw = {'marker': 'o', 'linestyle': 'None', 'markersize': 5}
    kwargs_int = {'marker': None, 'linestyle': '--'}

    axes.plot(curve1.i, curve1.j, label='Side 1', color='red', **kwargs_raw)
    axes.plot(curve2.i, curve2.j, label='Side 2', color='orange', **kwargs_raw)

    # Plot interpolated values
    axes.plot(curve1i.i,
              curve1i.j,
              label='Side 1: interp',
              color='darkred',
              **kwargs_int)
    axes.plot(curve2i.i,
              curve2i.j,
              label='Side 2: interp',
              color='orange',
              **kwargs_int)

    # Add extras
    plot_extras(axes, equal=True, show=True)


def plot_curves(curves):

    kwargs_raw = {'marker': 'o', 'linestyle': '-', 'markersize': 5}

    # Define plot
    axes = plt.gca()

    for ix, curve in enumerate(curves):

        axes.plot(curve.i, curve.j, label=f"Raw: {ix}", **kwargs_raw)

        for ix in range(len(curve)):
            axes.annotate(ix, (curve.i[ix], curve.j[ix]))

    # Add extras
    plot_extras(axes, equal=True, show=True)


''' Parent Methods: Transformations '''

def test_scaling_(curve):
    """Non uniform scaling """
    # Scale about default
    curve_t = curve.scale(1.3)
    curve_t = curve.scale((1.3))
    curve_t = curve.scale((1.3, 0.25, 2.0))
    curve_t = curve.scale([1.5, 0.15, 2.0])

    # Scale about point
    curve_t = curve.scale(1.3, center=Point(0.40, 0, 0))
    curve_t = curve.scale((1.3), center=Point(0.40, 0, 0))
    curve_t = curve.scale([1.3, 0.25, 0], center=Point(0.40, 0, 0))
    curve_t = curve.scale((1.3, 0.25, 0), center=Point(0.40, 0, 0))

    # Incorrect number of scale values
    curve_t = curve.scale([1.3], center=Point(0.40, 0, 0))
    curve_t = curve.scale([1.3, 2.0], center=Point(0.40, 0, 0))
    curve_t = curve.scale([1.3, 2.0, 2, 3], center=Point(0.40, 0, 0))


def check_rotation(foil: Airfoil, method: str, center=None, n: int=10):
    """Visually check the rotation of an airfoil
    
    Args:
        foil (Airfoil): airfoil to manipulate
        method (str): transformation method
        center (str or Point): center of rotation
        n (int): number of transformations
    """

    # Plot the curve
    # axes = foil.plot_curve(color='black', marker='o', label='Original')
    axes = plt.gca()

    # Generate colors and random point
    colors = get_colors(n)

    # Grab appropriate coordinates
    if method not in set(['rotate_i', 'rotate_j', 'rotate_k']):
        raise ValueError(f"Unexpected method type: {method}, expecting 'rotate_i, rotate_j or rotate_k'")

    # No center of rotation provided
    if center is None:
        pt = Point(*np.random.rand(3))

    elif isinstance(center, Point):
        pt = center

    elif center == LE:
        pt = foil.le

    elif center == TE:
        pt = foil.te

    else:
        raise ValueError(
            "Unexpected value {center}. Expecting (None, Point, {LE} or {TE})."
        )

    # Create series of offsets
    for ix, rot in enumerate(np.linspace(0, np.pi / 2, n)):

        # Create new curve and plot
        foil_i = getattr(foil, method)(rot, pt)
        axes = foil_i.plot_curve(axes,
                          marker='o',
                          label=None,
                          color=colors[ix])

    plot_extras(axes, xlabel='i', ylabel='j', title=f'Rotation: {method}', equal=True, show=True)


def check_scale_uniform(foil: Airfoil, center=None, n: int=10):
    """Visually check the rotation of an airfoil
    
    Args:
        foil (Airfoil): airfoil to manipulate
        center (str or Point): center of rotation
        n (int): number of transformations
    """

    # Plot the curve
    # axes = foil.plot_curve(color='black', marker='o', label='Original')
    axes = plt.gca()

    # Generate colors and random point
    colors = get_colors(n)

    # No center of rotation provided
    if center is None:
        pt = Point(*np.random.rand(3))

    elif isinstance(center, Point):
        pt = center

    elif center == LE:
        pt = foil.le

    elif center == TE:
        pt = foil.te

    else:
        raise ValueError(
            "Unexpected value {center}. Expecting (None, Point, {LE} or {TE})."
        )

    # Create series of offsets
    for ix, val in enumerate(np.linspace(0, 2, n)):

        # Create new curve and plot
        foil_i = getattr(foil, 'scale')(val, pt)
        axes = foil_i.plot_curve(axes,
                          marker='o',
                          label=None,
                          color=colors[ix])

    plot_extras(axes, xlabel='i', ylabel='j', title='Scale', equal=True, show=True)


def check_scale_nonuniform(foil: Airfoil, dim: str, center=None, n: int=5):
    """Visually check the rotation of an airfoil
    
    Args:
        foil (Airfoil): airfoil to manipulate
        dim (str): dimension for scaling
        center (str or Point): center of rotation
        n (int): number of transformations
    """

    # Plot the curve
    # axes = foil.plot_curve(color='black', marker='o', label='Original')
    axes = plt.gca()

    # Generate colors and random point
    colors = get_colors(n)

    # No center of rotation provided
    if center is None:
        pt = Point(*np.random.rand(3))

    elif isinstance(center, Point):
        pt = center

    elif center == LE:
        pt = foil.le

    elif center == TE:
        pt = foil.te

    else:
        raise ValueError(
            "Unexpected value {center}. Expecting (None, Point, {LE} or {TE})."
        )

    jx = Curve.get_dim_index(dim)

    # Create series of offsets
    for ix, val in enumerate(np.linspace(0.25, 2, n)):

        # Define scaling array
        scl = [1.0, 1.0, 1.0]
        scl[jx] = val

        # Create new curve and plot
        foil_i = getattr(foil, 'scale')(scl, pt)
        axes = foil_i.plot_curve(axes,
                                 marker='o',
                                 label=None,
                                 color=colors[ix])

    plot_extras(axes,
                xlabel='i',
                ylabel='j',
                title='Scale',
                equal=True,
                show=True)


def check_translation(foil: Airfoil, dim: str, n: int = 20):
    """Visually check the scaling of an airfoil
    
    Args:
        foil (Airfoil): base airfoil
        dim (str): dimension for translation
        n (int): number of points to check
    """

    # Plot the curve
    axes = foil.plot_curve(color='black', marker='o', label='Original')

    # Generate colors and random point
    colors = get_colors(n)

    # Create random base offset
    pt = Point(*np.random.rand(3))
    delta = np.zeros(3)
    jx = Curve.get_dim_index(dim)

    # Create series of offsets
    for ix, scl in enumerate(np.linspace(0, 1, n)):

        # Modify dimension of interest
        delta[jx] = scl

        # Create new curve and plot
        foil_i = getattr(foil, 'translate')(*delta)
        axes = foil_i.plot_curve(axes,
                          marker='o',
                          label=None,
                          color=colors[ix])

    plot_extras(axes, xlabel='i', ylabel='j', title=f'Translation: {dim}', equal=True, show=True)


''' Airfoil: Orientation Checks '''

def check_orientation_vertical(foil: Airfoil):
    """Check the airfoil orientation for a vertical mismatch"""
    pass


def check_orientation_horizontal(foil: Airfoil):
    """Check the airfoil orientation for a horizontal mismatch"""
    pass


def check_orientation_rotational(foil: Airfoil):
    """Check the airfoil orientation for a rotational mismatch"""
    pass


''' Airfoil: Parameters '''

def check_centroid(foil: Airfoil):
    """Visually check the centroid of an airfoil"""

    # Plot original
    axes = foil.plot_curve(label='Airfoil')

    # Compute centroid
    centroid = foil.centroid()
    axes.plot(centroid.i, centroid.k, marker='o', color='pink', ms=8)

    # Add extras
    plot_extras(axes, equal=True)


''' Airfoil Miscellaneous '''

def test_combined_curve(curve1: Curve2D, curve2: Curve2D):
    """Combine two sides of curve """
    # Interpolate airfoils for fidelity
    # curve1 = curve1.interpolate_by_s(200, show=True)
    # curve2 = curve2.interpolate_by_s(200, show=True)

    # Combine curve manually
    curve_c = Curve2D(curve1.points + curve2.points[::-1])
    curve_c = curve_c.interpolate_by_s(n=200, show=True)

    # Plot curve
    axes = curve_c.plot(label='Original', marker='o')
    # axes = curve_c.draw_curvature_normal_lines(1.0, axes, color='pink')
    # plot_extras(axes, equal=True, show=True)


def test_foil(curve1: Curve2D, curve2: Curve2D):

    # Interpolate airfoils for fidelity
    curve1 = curve1.interpolate_by_s(1000)
    curve2 = curve2.interpolate_by_s(1000)

    # Reverse to avoid head to tail line up
    curve2.reverse()

    # Correct orientation
    foil = Airfoil(curve1, curve2)

    # Airfoil thickness
    # foil.thickness()
    axes = foil.plot_curve(label='Airfoil')
    axes = foil.plot_curvature(axes, scale=-0.05, label='Curvature')

    # Add extras
    plot_extras(axes, equal=True)


def build_foil() -> Airfoil:
    """Ease of use function"""

    # Load data
    curve1 = Curve2D.from_file(os.path.join('tests', 'inputs', 'naca65115_1.txt'))
    curve2 = Curve2D.from_file(os.path.join('tests', 'inputs', 'naca65115_2.txt'))

    # Interpolate airfoils for fidelity
    curve1 = curve1.interpolate_by_s(500)
    curve2 = curve2.interpolate_by_s(500)

    # Reverse to avoid head to tail line up
    curve2.reverse()

    # Correct orientation
    foil = Airfoil(curve1, curve2)

    return foil


if __name__ == "__main__":

    print(__file__)

    # Build it
    foil = build_foil()

    # Transformation check: rotation
    # check_rotation(foil, 'rotate_k', center=LE)
    # check_rotation(foil, 'rotate_k', center=TE)
    # check_rotation(foil, 'rotate_k', center=None)

    # Transformation check: uniform scale
    # check_scale_uniform(foil, center=LE)
    # check_scale_uniform(foil, center=TE)
    # check_scale_uniform(foil, center=None)

    # Transformation check: non uniform scale about LE
    # check_scale_nonuniform(foil, dim='i', center=LE)
    # check_scale_nonuniform(foil, dim='j', center=LE)
    # check_scale_nonuniform(foil, dim='k', center=LE)
    
    # Transformation check: non uniform scale about TE
    # check_scale_nonuniform(foil, dim='i', center=TE)
    # check_scale_nonuniform(foil, dim='j', center=TE)
    # check_scale_nonuniform(foil, dim='k', center=TE)

    # Transformation check: non uniform scale about random
    # check_scale_nonuniform(foil, dim='i', center=None)
    # check_scale_nonuniform(foil, dim='j', center=None)
    # check_scale_nonuniform(foil, dim='k', center=None)

    # Transformation check: translation
    # check_translation(foil, 'i', 20)
    # check_translation(foil, 'j', 20)
    # check_translation(foil, 'k', 20)

    # Plot curvature
    axes = foil.plot_curve()
    # axes = foil.plot_curvature(axes, scale=0.25)
    # axes = foil.plot
    plot_extras(axes, equal=True)

    # Run airfoil parameter checks
    # check_centroid(foil)

    # test_combined_curve(curve1, curve2)
    # test_foil(curve1, curve2)

    # Incorrect orientation: vertical
    # foil = Airfoil(curve2, curve1)

    # Incorrect orientation: horizontal
    # curve2.reverse()
    # foil = Airfoil(curve1, curve2)
    quit()

    # Interpolation
    # curve1 = curve1.interpolate_by_s(1000)
    # curve2 = curve2.interpolate_by_s(1000)

    # Sort curve

    # Get split indices
    # ix_min = np.argmin(curve.i)
    # ix_max = np.argmax(curve.i)

    # Split the curve
    # curve1 = Curve(curve.points[ix_min:ix_max + 1])
    # curve2 = Curve(curve.points[ix_max:] + curve.points[:ix_min])

    # Plot data
    # axes.plot(curve.i, curve.j, label='raw', marker='o', color='black')
    # axes.plot(curve_t.i, curve_t.j, label='translate', marker='o', color='green')

    # Plot raw data
    plot_curves([curve1, curve2])
    # plot_extras(axes, equal=False, show=True)

    # Interpolate
    # plot_interpolate_airfoil(curve1, curve2)

    # Calculate camberline
    # calculate_camberline(curve1, curve2)
    # calculate_camberline2(curve1, curve2)

    # todo: generate report
    # - original raw
    # - split curve
    # - interpolated vs raw
    # - mean camberline
    # - projected LE, and TE?
    # - thickness calculation, normalized distance from LE

    # alternative methods
    # spline and line fit
    # - https://stackoverflow.com/questions/234261/the-intersection-point-between-a-spline-and-a-line
