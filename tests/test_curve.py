from copy import deepcopy
import numpy as np
import os
import sys
from typing import List
import unittest

import hypothesis.strategies as st
from hypothesis import given

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from geometry import Point, Curve

# from geometry import DIST_NORM, SPL_C
from utilities_curve import build_cosine_wave


class TestInitialization(unittest.TestCase):
    """Test class initialization
    References:
        https://docs.python.org/3/library/unittest.html#assert-methods
        https://docs.pytest.org/en/latest/index.html
        https://hypothesis.readthedocs.io/en/latest/data.html
    """

    """ Acceptable Inputs """

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_by_points(self, args):
        """Initialize curve using points"""

        pts: list = []

        for pt in args:
            print(pt)
            pts.append(Point(*pt))

        crv = Curve(pts)

        # self.assertIsInstance(Curve(pts), Curve)
        self.assertTrue(isinstance(crv, Curve))

    def test_init_from_file(self):
        """Initialize curve from sample input files"""
        # TODO: dynamically generate a gambit of files and test those

        # Grab test files
        dir_src: str = "inputs"
        files: List[str] = os.listdir(dir_src)

        # Check initialization for each test
        for file in files:
            with self.subTest(file):
                self.assertIsInstance(
                    Curve.from_file(os.path.join(dir_src, file)), Curve
                )

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_from_values(self, args):
        """Initialize curve using input iterable"""
        self.assertIsInstance(Curve.from_values(args), Curve)

    """ Unacceptable Inputs """

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_by_values_with_null_value(self, args):
        """Try to initialize Curve with mainly values, and a single invalid value"""

        # Pick a random index and nullify
        ix = np.random.randint(0, len(args))
        args[ix] = None

        with self.assertRaises(TypeError):
            crv = Curve.from_values(args)


class TestProperties(unittest.TestCase):
    """Test basic properties"""

    def setUp(self):
        # Create basic curve
        self.crv = build_cosine_wave(np.linspace(0, 2.0 * np.pi, 200), ctype=Curve)

    """ Basic properties """
    # TODO: get item ie: singular, multiple, slices
    # TODO: set item ie: singular, multiple, slices
    # TODO: __repr__ test if strict definition is necessary

    """ Dimensionality """

    @given(
        st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=2),
        st.integers(min_value=3, max_value=500)
    )
    def test_dimension_of_1(self, const: List[float], n: int):
        """Verify curve varies in one dimension

        Args:
            const (List[float]): two constant numeric values
            n (int): number of points in curve
        """

        # Choose a dimension for variability
        for ix in range(3):

            # Generate random array
            arr = np.random.rand(n, 3) * np.random.randint(-1e5, 1e5 + 1, dtype='int64')

            label = f"Dimension: {ix}"

            # Remove dimension with variability
            dims = set([0, 1, 2])
            dims.remove(ix)

            # Remaining dimensions
            for jx, val in zip(dims, const):

                # Set dimension to a constant value
                arr[:, jx] = val

            with self.subTest(label):

                # Create curve and verify a single dimension
                crv = Curve.from_values(arr)
                self.assertEqual(1, crv.dim)

    @given(
        st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=2),
        st.integers(min_value=3, max_value=500)
    )
    def test_dimension_of_2(self, const: List[float], n: int):
        """Verify curve varies in two dimension

        Args:
            const (List[float]): two constant numeric values
            n (int): number of points in curve
        """

        # Choose a dimension for variability
        for ix in range(3):

            # Generate random array
            arr = np.random.rand(n, 3) * np.random.randint(-1e5, 1e5 + 1, dtype='int64')

            # Set dimension to a constant value
            arr[:, ix] = np.random.rand() * np.random.randint(-1e5, 1e5 + 1, dtype='int64')

            label = f"Dimension: {ix}"

            with self.subTest(label):

                # Create curve and verify dimensionality
                crv = Curve.from_values(arr)
                self.assertEqual(2, crv.dim)

    @given(st.integers(min_value=3, max_value=500))
    def test_dimension_of_3(self, n: int):
        """Verify curve varies in three dimensions

        Args:
            n (int): number of points in curve
        """

        # Generate random array and create curve
        arr = np.random.rand(n, 3) * np.random.randint(-1e5, 1e5 + 1, dtype='int64')
        crv = Curve.from_values(arr)
        self.assertEqual(3, crv.dim)

    """ Coordinate Arrays """

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_dimension_getter(self, args):
        """Verify curve acquires proper data set in all dimensions"""

        # Generate points
        crv = Curve.from_values(args)

        for dim in ["i", "j", "k"]:
            with self.subTest(dim):
                arr = [getattr(pt, dim) for pt in crv.points]
                self.assertEqual(getattr(crv, dim), arr)

    """ Start & Stop """

    def test_start_getter(self):
        """Grab values from the first point"""
        self.assertEquals(self.crv[0].coords, self.crv.start.coords)

    def test_stop_getter(self):
        """Grab values from the last point"""
        self.assertEquals(self.crv[-1].coords, self.crv.stop.coords)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_start_setter(self, args):
        """Set the first point and verify update"""

        # Update first point
        crv = deepcopy(self.crv)
        crv.start = Point(*args)
        self.assertEquals(args, crv.start.coords)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_stop_setter(self, args):
        """Set the last point and verify update"""

        # Update last point
        crv = deepcopy(self.crv)
        crv.stop = Point(*args)
        self.assertEquals(args, crv.stop.coords)

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_points_setter(self, args):
        """Update curve with new set of points"""

        # Modify for easy math
        args = np.array([np.array(val) for val in args])

        # Create base curve and modified curve
        crv = Curve.from_values(args)
        crv2 = Curve.from_values(args * np.random.random_sample())

        # Update base curve
        setattr(crv, "points", crv2.points)

        # Verify update
        check = all([crv[ix] == crv2[ix] for ix in range(len(crv))])
        self.assertTrue(check)


class TestObservation(unittest.TestCase):
    """Test updates from the Subject & Observer relationship"""

    def setUp(self):
        # todo: use a gambit of test curves

        # Create a base curve
        self.crv = build_cosine_wave(np.linspace(0, 2.0 * np.pi, 200), ctype=Curve)

    """ Observer Update """

    def test_initial_state(self):
        """Verify null state for initial curve"""

        # Grab new state
        state = self.crv.get_state()

        # Check that all values are none
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

        @given(st.floats(allow_nan=False, allow_infinity=False))
        def test_point_update_single_dimension(self, arg):
            """Update a random point: in one dimension"""

            # For every dimension
            for dim in ["i", "j", "k"]:

                # Create a copy of the current curve
                crv = deepcopy(self.crv)

                # Calculate parameters
                _ = crv.s
                crv.fit_splines()

                # Update dimension of random point
                ix = np.random.randint(0, len(crv))
                setattr(crv[ix], dim, arg)

                # Grab new state
                state = crv.get_state()

                with self.subTest(dim):

                    # Check that all values were reset
                    check = all(
                        [value == None.__class__.__name__ for value in state.values()]
                    )
                    self.assertTrue(check)

    def test_update(self):
        """Update the curve manually"""

        # Create a copy of the current curve
        crv = deepcopy(self.crv)

        # Calculate parameters
        _ = crv.s
        crv.fit_splines()

        # Update curve
        crv.update()

        # Grab new state
        state = crv.get_state()

        # Check that all values were reset
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        )
    )
    def test_update_with_new_point(self, args):
        """Update a random point: all dimensions"""

        # Create a copy of the current curve
        crv = deepcopy(self.crv)

        # Calculate parameters
        _ = crv.s
        crv.fit_splines()

        # Update a random point with new data
        ix = np.random.randint(0, len(crv))
        crv[ix] = Point(*args)

        # Grab new state
        state = crv.get_state()

        # Check that all values were reset
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

    @given(st.floats(allow_nan=False, allow_infinity=False))
    def test_update_of_point_dimension(self, arg):
        """Update a random point: in one dimension"""

        # For every dimension
        for dim in ["i", "j", "k"]:

            # Create a copy of the current curve
            crv = deepcopy(self.crv)

            # Calculate parameters
            _ = crv.s
            crv.fit_splines()

            # Update dimension of random point
            ix = np.random.randint(0, len(crv))
            setattr(crv[ix], dim, arg)

            # Grab new state
            state = crv.get_state()

            with self.subTest(dim):

                # Check that all values were reset
                check = all(
                    [value == None.__class__.__name__ for value in state.values()]
                )
                self.assertTrue(check)
