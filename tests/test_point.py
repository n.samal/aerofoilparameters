import unittest
import operator
import os
import numpy as np
import sys
from functools import reduce

import hypothesis.strategies as st
from hypothesis import given
from hypothesis.extra.numpy import arrays

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from geometry import Point


class TestInitialization(unittest.TestCase):
    """Test class initialization
    References:
        https://docs.python.org/3/library/unittest.html#assert-methods
        https://docs.pytest.org/en/latest/index.html
        https://hypothesis.readthedocs.io/en/latest/data.html
    """

    """ Acceptable Inputs """

    @given(st.lists(st.integers(), min_size=3, max_size=3))
    def test_list_ints(self, args):
        """List - Integer input"""
        pt = Point(*args)
        self.assertEqual(args, pt.coords)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_list_floats(self, args):
        """List - Float input"""
        pt = Point(*args)
        self.assertEqual(args, pt.coords)

    @given(
        st.tuples(
            st.floats(allow_nan=False, allow_infinity=False),
            st.floats(allow_nan=False, allow_infinity=False),
            st.floats(allow_nan=False, allow_infinity=False),
        )
    )
    def test_tuple_floats(self, args):
        """Tuple - Float input"""
        pt = Point(*args)
        self.assertEqual(list(args), pt.coords)

    @given(
        arrays(np.float, 3, elements=st.floats(allow_nan=False, allow_infinity=False))
    )
    def test_array(self, args):
        """Tuple - Numpy array input"""
        pt = Point(*args)
        self.assertEqual(list(args), pt.coords)

    """ Unacceptable inputs """

    @given(st.lists(st.integers(), min_size=0, max_size=1))
    def test_list_ints_partial(self, args):
        """List of ints - not enough inputs"""
        with self.assertRaises(TypeError):
            Point(*args)

    @given(st.lists(st.none(), min_size=2, max_size=3))
    def test_list_none(self, args):
        """Pass all strings"""
        with self.assertRaises(TypeError):
            Point(*args)

    @given(st.lists(st.text(), min_size=2, max_size=3))
    def test_list_string(self, args):
        """List of strings"""
        with self.assertRaises(TypeError):
            Point(*args)

    @given(st.lists(st.complex_numbers(), min_size=3, max_size=3))
    def test_list_complex(self, args):
        """Pass other invalid types"""
        with self.assertRaises(TypeError):
            Point(*args)


class TestMathOperators(unittest.TestCase):
    """Test point operators

    References:
        https://docs.python.org/3/library/unittest.html#assert-methods
    """

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_addition(self, args1, args2):
        """Addition operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        pt_new = operator.add(pt1, pt2)
        self.assertEqual(pt_new.coords, list(map(operator.add, pt1, pt2)))

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_subtraction(self, args1, args2):
        """Subtraction operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        pt_new = operator.sub(pt1, pt2)
        self.assertEqual(pt_new.coords, list(map(operator.sub, pt1, pt2)))

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_multiplication(self, args1, args2):
        """Multiplication operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        pt_new = operator.mul(pt1, pt2)
        self.assertEqual(pt_new.coords, list(map(operator.mul, pt1, pt2)))


class TestComparisonOperators(unittest.TestCase):
    """Test point comparison operators"""

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_greater_than(self, args1, args2):
        """Greater than operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        self.assertEqual(operator.gt(pt1, pt2), list(map(operator.gt, pt1, pt2)))

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_greater_than_equal(self, args1, args2):
        """Greater than or equal operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        self.assertEqual(operator.ge(pt1, pt2), list(map(operator.ge, pt1, pt2)))

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_less_than(self, args1, args2):
        """Less than operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        self.assertEqual(operator.lt(pt1, pt2), list(map(operator.lt, pt1, pt2)))

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
    )
    def test_less_than_equal(self, args1, args2):
        """Less than or equal operation"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        self.assertEqual(operator.le(pt1, pt2), list(map(operator.le, pt1, pt2)))


class TestProperties(unittest.TestCase):
    """Test basic properties"""

    """ Valid Operations """

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.floats(allow_nan=False, allow_infinity=False),
    )
    def test_set_ijk(self, args, val):
        """Set property: i,j,k"""

        pt = Point(*args)

        for ix, char in enumerate(["i", "j", "k"]):

            with self.subTest(char):
                setattr(pt, char, val)
                self.assertEqual(pt.coords[ix], val)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_get_ijk(self, args):
        """Get property: i,j,k"""

        pt = Point(*args)

        for ix, char in enumerate(["i", "j", "k"]):

            with self.subTest(char):
                self.assertEqual(getattr(pt, char), args[ix])

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_coords_getter(self, args):
        """Test property: get coords"""
        pt = Point(*args)
        self.assertEqual(pt.coords, args)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
    )
    def test_coords_setter(self, args1, args2):
        """Test property: set coords"""

        # Original
        pt = Point(*args1)

        # Attempt update
        pt.coords = args2

        self.assertEqual(pt.coords, args2)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        )
    )
    def test_item_getter(self, args):
        """Test property: get item"""

        # Original
        pt = Point(*args)

        for ix in range(3):

            with self.subTest(ix):
                self.assertEqual(pt[ix], args[ix])

    """ Invalid Operations """

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
    )
    def test_item_setter(self, args1, args2):
        """Test property: set coords"""

        # Original point
        pt = Point(*args1)

        for ix in range(3):
            with self.subTest(ix):
                with self.assertRaises(TypeError):

                    # Try to set new points with short hand
                    pt[ix] = args2[ix]


class TestFunctions(unittest.TestCase):
    """Test functions in point class

    References:
        https://docs.scipy.org/doc/scipy/reference/linalg.html
    """

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        )
    )
    def test_magnitude(self, args):
        """Test magnitude method"""

        pt = Point(*args)
        # mag = np.sqrt(reduce(operator.add, [v**2 for v in pt]))
        mag = np.sqrt(np.sum(np.square(args)))
        self.assertEquals(pt.mag(), mag)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
    )
    def test_distance(self, args1, args2):
        """Test distance method"""

        pt1 = Point(*args1)
        pt2 = Point(*args2)
        dist = np.sqrt(np.sum(np.square(np.array(args1) - np.array(args2))))
        # dist = np.sqrt(reduce(operator.add, [(v1 - v2)**2 for v1, v2 in zip(pt1, pt2)]))
        self.assertEqual(pt1.dist(pt2), dist)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=3, max_size=3
        ),
    )
    def test_translate(self, args1, args2):
        """Test translation method"""

        # Define point and translate
        pt1 = Point(*args1)
        pt_new = pt1.translate(*args2)

        # Manual check
        arr = np.array(args1) + np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.floats(
            allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
        ),
    )
    def test_scale_scalar(self, args1, args2, scale):
        """Test scale method with a singular value"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.scale(scale, Point(*args2))

        # Manual method
        arr = (np.array(args1) - np.array(args2)) * scale
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
    )
    def test_scale_iter_lists(self, args1, args2, scales):
        """Test scalar method with multiple scales: lists"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.scale(scales, Point(*args2))

        # Manual check
        arr = (np.array(args1) - np.array(args2)) * np.array(scales)
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.tuples(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
        ),
    )
    def test_scalar_iter_tuple(self, args1, args2, scales):
        """Test scalar method with multiple scales: lists"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.scale(scales, Point(*args2))

        # Manual check
        arr = (np.array(args1) - np.array(args2)) * np.array(scales)
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
    )
    def test_reflect_i(self, args1, args2):
        """Test reflection method across i"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.reflect_i(Point(*args2))

        # Manual check
        arr = np.array(args1) - np.array(args2)
        arr[0] = arr[0] * -1
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
    )
    def test_reflect_j(self, args1, args2):
        """Test reflection method across j"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.reflect_j(Point(*args2))

        # Manual check
        arr = np.array(args1) - np.array(args2)
        arr[1] = arr[1] * -1
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
    )
    def test_reflect_k(self, args1, args2):
        """Test reflection method across k"""

        # Class methods
        pt = Point(*args1)
        pt_new = pt.reflect_k(Point(*args2))

        # Manual check
        arr = np.array(args1) - np.array(args2)
        arr[2] = arr[2] * -1
        arr += np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.floats(
            allow_nan=False,
            allow_infinity=False,
            min_value=-2.0 * np.pi,
            max_value=2.0 * np.pi,
        ),
    )
    def test_rotate_i(self, args1, args2, angle):
        """Test rotation about i axis at a given point"""

        print("args1:", args1)
        print("args2:", args2)
        print("angle:", angle)

        # Define point and scale
        pt = Point(*args1)
        pt_new = pt.rotate_i(angle, Point(*args2))

        # Shift point to center
        arr = np.array(args1) - np.array(args2)

        # Rename for easier understanding
        i, j, k = arr

        # Apply transform
        arr = [
            i,
            j * np.cos(angle) - k * np.sin(angle),
            j * np.sin(angle) + k * np.cos(angle),
        ]

        # Shift point back
        arr = np.array(arr) + np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.floats(
            allow_nan=False,
            allow_infinity=False,
            min_value=-2.0 * np.pi,
            max_value=2.0 * np.pi,
        ),
    )
    def test_rotate_j(self, args1, args2, angle):
        """Test rotation about j axis at a given point"""

        # Define point and scale
        pt = Point(*args1)
        pt_new = pt.rotate_j(angle, Point(*args2))

        # Shift point to center
        arr = np.array(args1) - np.array(args2)

        # Rename for easier understanding
        i, j, k = arr

        # Apply transform
        arr = [
            i * np.cos(angle) + k * np.sin(angle),
            j,
            -i * np.sin(angle) + k * np.cos(angle),
        ]

        # Shift point back
        arr = np.array(arr) + np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())

    @given(
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.lists(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=-1e10, max_value=1e10
            ),
            min_size=3,
            max_size=3,
        ),
        st.floats(
            allow_nan=False,
            allow_infinity=False,
            min_value=-2.0 * np.pi,
            max_value=2.0 * np.pi,
        ),
    )
    def test_rotate_k(self, args1, args2, angle):
        """Test rotation about k axis at a given point"""

        # Define point and scale
        pt = Point(*args1)
        pt_new = pt.rotate_k(angle, Point(*args2))

        # Shift point to center
        arr = np.array(args1) - np.array(args2)

        # Rename for easier understanding
        i, j, k = arr

        # Apply transform
        arr = [
            i * np.cos(angle) - j * np.sin(angle),
            i * np.sin(angle) + j * np.cos(angle),
            k,
        ]

        # Shift point back
        arr = np.array(arr) + np.array(args2)

        self.assertEqual(pt_new.coords, arr.tolist())


if __name__ == "__main__":

    unittest.main()
