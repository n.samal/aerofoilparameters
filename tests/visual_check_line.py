import colorsys
import numpy as np
from matplotlib import pyplot as plt
import os
import sys

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from utilities import plot_extras, get_colors
from geometry import Point, Line, Line2D, Line3D

''' Parent Methods '''

def check_reflect(method: str, line: Line=None, n: int=50):
    """Check the reflection method via plot

    Args:
        method (str): method to apply
        line (Line): line to transform
        n (int): number of points to reflect
    Returns:
        None
    Todo:
        - plot in 3D space as well
    """

    print(f' Checking reflect: {method}')

    # Create random lines
    if line is None:

        # Initialize random line
        line = Line3D([Point(*np.random.random(3)), Point(*np.random.random(3))])

        # Generate offsets of this line
        if method == 'reflect_i':
            lines = [line.translate(0, t, 0) for t in np.linspace(0, 1, n)]
        elif method == 'reflect_j':
            lines = [line.translate(0, 0, t) for t in np.linspace(0, 1, n)]
        elif method == 'reflect_k':
            lines = [line.translate(t, 0, 0) for t in np.linspace(0, 1, n)]

    # Create basic axis
    axes = plt.gca()
    kwargs = {'ms': 4, 'linewidth': 3, 'marker': 'o'}

    colors = get_colors(n)

    for ix, line in enumerate(lines):

        # Apply method
        line_i = getattr(line, method)(center=line.start)

        # Grab appropriate coordinates
        if method == 'reflect_i':
            x = 'i'
            y = 'j'
        elif method == 'reflect_j':
            x = 'j'
            y = 'k'
        elif method == 'reflect_k':
            x = 'k'
            y = 'i'
        else:
            raise ValueError(f"Unexpected method type: {method}, expecting 'reflect_i, reflect_j or reflect_k'")

        # Plot current line
        line.plot(axes, x, y, color=colors[ix], **kwargs)
        line_i.plot(axes, x, y, color=colors[ix], **kwargs)

    # Add extras
    plot_extras(axes, equal=True, title=method + ' check', xlabel=x, ylabel=y)


def check_rotate(method: str, line: Line=None, n: int=50, annotate: bool=False):
    """Check the rotation method via plot

    Rotate the line about the starting point
    of a line to see

    Args:
        method (str): method to apply
        line (Line): line to rotate
        n (int): number of splits about 2pi
        annotate (bool): show me those indices
    Returns:
        None
    Todo:
        - plot in 3D space as well
    """

    print(f' Checking rotation: {method}')

    if line is None:

        # Generate random points
        args1 = np.random.random(3)
        args2 = np.random.random(3)

        # Create a line
        # line = Line2D([Point(*args1), Point(*args2)])
        line = Line3D([Point(*args1), Point(*args2)])

    # Create basic axis
    axes = plt.gca()
    kwargs = {'ms': 4, 'linewidth': 3, 'marker': 'o'}

    colors = get_colors(n)

    for ix, angle in enumerate(np.linspace(0, np.pi * 2., n)):

        # Apply method
        line_i = getattr(line, method)(center=line.start, angle=angle)

        # Grab appropriate coordinates
        if method == 'rotate_i':
            x = 'j'
            y = 'k'
        elif method == 'rotate_j':
            x = 'i'
            y = 'k'
        elif method == 'rotate_k':
            x = 'i'
            y = 'j'
        else:
            raise ValueError(f"Unexpected method type: {method}, expecting 'rotate_i, rotate_j or rotate_k'")

        # Plot current line
        line_i.plot(axes, x, y, color=colors[ix], **kwargs)

        if annotate:
            axes.annotate(ix, (getattr(line_i.stop, x), getattr(line_i.stop, y)))

    # Add extras
    plot_extras(axes, equal=True, title=method + ' check', xlabel=x, ylabel=y)


def check_translate(dim: str,
                    line: Line = None,
                    n: int = 50,
                    annotate: bool = False):
    """Apply translation to a line in several directions

    Args:
        dim (str): dimension to apply transformation
        line (Line): line to rotate
        n (int): number of splits
        annotate (bool): show me those indices
    Returns:
        None
    Todo:
        - apply for 3D point
        - plot in 3D space as well
    """

    print(' Checking translate')

    if line is None:

        # Create a line
        line = Line3D([Point(*np.random.random(3)), Point(*np.random.random(3))])

    # Create basic axis
    axes = plt.gca()
    kwargs = {'ms': 4, 'linewidth': 3, 'marker': 'o'}

    colors = get_colors(n)

    for ix, t in enumerate(np.linspace(1, 2, n)):

        # Apply method
        line_i = line.translate(0, t, 0)

        x = 'i'
        y = 'j'

        # Plot current line
        line_i.plot(axes, x, y, color=colors[ix], **kwargs)

        if annotate:
            axes.annotate(ix, (getattr(line_i.stop, x), getattr(line_i.stop, y)))

    # Add extras
    plot_extras(axes, equal=True, title='translate check', xlabel=x, ylabel=y)


''' Parent Methods: Manual Checks '''

def check_rotate_k_manual():
    """Apply translation on known line with annotation"""

    # Rotation check: manual
    start = Point(1, 1)
    stop = Point(2, 1)
    line = Line2D([start, stop])
    check_rotate('rotate_k', line, n=9, annotate=True)


def check_translate_manual():
    """Apply translation on known line with annotation"""

    # Define known lines
    start = Point(1, 1)
    stop = Point(2, 1)
    line = Line2D([start, stop])

    check_translate(line, n=5, annotate=True)


''' Line2D Checks '''

def check_normal(line1: Line, line2: Line, ind: int=1):
    """Plot lines and potential intersections

    Args:
        line1 (Line): 1st line
        line2 (Line): 2nd line
        ind (int): indentation prior to output
    Returns:
        None
    """

    print(f"{' '*ind}Checking normal")

    # Compute normals
    line1_normal = line1.normal_line()
    line2_normal = line2.normal_line()

    # Compute dot product between lines
    dot1 = np.dot(line1.vector, line1_normal.vector)
    dot2 = np.dot(line2.vector, line2_normal.vector)

    print(f"{' '*ind} dot1: {dot1}")
    print(f"{' '*ind} dot2: {dot2}")

    # line2 = Line2D([Point(2, 1), Point(2.3, 2.5)])

    # Define plot
    axes = plt.gca()

    # Plot lines
    kwargs_raw = {'ms': 4, 'linewidth': 3, 'marker': 'o'}
    kwargs_nrm = {'ms': 4, 'linewidth': 3, 'marker': 'o', 'alpha': 0.50}

    axes.plot(line1.i, line1.j, color='black', label='line 1', **kwargs_raw)
    axes.plot(line2.i, line2.j, color='blue', label='line 2', **kwargs_raw)

    axes.plot(line1_normal.i,
              line1_normal.j,
              color='black',
              label='normal: line 1',
              **kwargs_nrm)
    axes.plot(line2_normal.i,
              line2_normal.j,
              color='blue',
              label='normal: line 2',
              **kwargs_nrm)

    # Compute intersection
    int_bool, int_pt = line1.intersection(line2)

    print(f"{' '*ind} intersection:", int_bool)

    if int_pt:
        axes.plot(int_pt.i, int_pt.j, color='red', **kwargs_raw)

    plot_extras(axes, equal=True)


''' Line2D: Manual Checks '''

def check_normals_manual():
    """Check three major cases for line normals"""

    print(' Checking normals manually')

    # No intersection
    print('  Checking intersection: none')
    line1 = Line2D([Point(-1, 2.5), Point(1, 2)])
    line2 = Line2D([Point(0, 1.5), Point(2, 1)])
    check_normal(line1, line2, 3)

    # Projected intersection
    print('  Checking intersection: projected')
    line1 = Line2D([Point(0, 0), Point(1, 1)])
    line2 = Line2D([Point(0, 1), Point(2, 5)])
    check_normal(line1, line2, 3)

    # Actual intersection
    print('  Checking intersection: actual')
    line1 = Line2D([Point(-1, 0), Point(1, 1)])
    line2 = Line2D([Point(-1, -1), Point(2, 5)])
    check_normal(line1, line2, 3)


if __name__ == "__main__":

    print(__file__)

    # Rotation checks
    # check_rotate('rotate_i', n=100)
    # check_rotate('rotate_j', n=100)
    # check_rotate('rotate_k', n=100)

    # Reflection checks
    # check_reflect('reflect_i', n=40)
    # check_reflect('reflect_j', n=40)
    # check_reflect('reflect_k', n=40)

    # Translation
    # check_translate(n=10, annotate=True)

    # Manual checks
    check_rotate_k_manual()
    # check_normals_manual()
