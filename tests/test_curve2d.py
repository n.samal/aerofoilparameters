from copy import deepcopy
import numpy as np
import os
import sys
from typing import List, Tuple
import unittest

import hypothesis.strategies as st
from hypothesis import given

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from geometry import Point, Curve2D
from utilities_curve import build_circle, build_cosine_wave

# TODO: observational update (by function, by_trigger1, by_trigger2)
# TODO: verify curvature of circle is near average of 1 / r
# TODO: verify evolute of a circle/ellipse is near 0, or specified center
# TODO: test slope using line segment between curve points
# TODO: test normals using line segment between points and 1 / m definition


class TestInitialization(unittest.TestCase):
    """Test class initialization
    References:
        https://docs.python.org/3/library/unittest.html#assert-methods
        https://docs.pytest.org/en/latest/index.html
        https://hypothesis.readthedocs.io/en/latest/data.html
    """

    """ Acceptable Inputs """

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_by_points(self, args):
        """Initialize curve using points"""

        pts: list = []

        for pt in args:
            print(pt)
            pts.append(Point(*pt))

        self.assertIsInstance(Curve2D(pts), Curve2D)

    def test_init_from_file(self):
        """Initialize curve from sample input files"""
        # TODO: dynamically generate a gambit of files and test those

        # Grab test files
        dir_src: str = "inputs"
        files: List[str] = os.listdir(dir_src)

        # Check initialization for each test
        for file in files:
            with self.subTest(file):
                self.assertIsInstance(
                    Curve2D.from_file(os.path.join(dir_src, file)), Curve2D
                )

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_from_values(self, args):
        """Initialize curve using input iterable"""
        self.assertIsInstance(Curve2D.from_values(args), Curve2D)

    """ Unacceptable Inputs """

    @given(
        st.lists(
            st.lists(
                st.floats(
                    allow_nan=False,
                    allow_infinity=False,
                    min_value=-1e10,
                    max_value=1e10,
                ),
                min_size=2,
                max_size=3,
            ),
            min_size=3,
            max_size=500,
        )
    )
    def test_init_by_values_with_null_value(self, args):
        """Try to initialize Curve with mainly values, and a single invalid value"""

        # Pick a random index and nullify
        ix = np.random.randint(0, len(args))
        args[ix] = None

        with self.assertRaises(TypeError):
            _ = Curve2D.from_values(args)


class TestObservation(unittest.TestCase):
    """Test updates from the Subject & Observer relationship"""

    def setUp(self):
        # todo: use a gambit of test curves

        # Create a base curve
        self.crv = build_cosine_wave(np.linspace(0, 2.0 * np.pi, 200), ctype=Curve2D)

    """ Observer Update """

    def test_initial_state(self):
        """Verify null state for initial curve"""

        # Grab new state
        state = self.crv.get_state()

        # Check that all values are none
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

    def test_update(self):
        """Update the curve manually"""

        # Create a copy of the current curve
        crv = deepcopy(self.crv)

        # Calculate parameters (Curve)
        _ = crv.s
        crv.fit_splines()

        # Calculate parameters (Curve2D)
        _ = crv.slopes
        _ = crv.normals

        # Update curve
        crv.update()

        # Grab new state
        state = crv.get_state()

        # Check that all values were reset
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

    @given(
        st.lists(
            st.floats(allow_nan=False, allow_infinity=False), min_size=2, max_size=3
        )
    )
    def test_update_with_new_point(self, args):
        """Update a random point: all dimensions"""

        # Create a copy of the current curve
        crv = deepcopy(self.crv)

        # Calculate parameters (Curve)
        _ = crv.s
        crv.fit_splines()

        # Calculate parameters (Curve2D)
        _ = crv.slopes
        _ = crv.normals

        # Update a random point with new data
        ix = np.random.randint(0, len(crv))
        crv[ix] = Point(*args)

        # Grab new state
        state = crv.get_state()

        # Check that all values were reset
        check = all([value == None.__class__.__name__ for value in state.values()])
        self.assertTrue(check)

    @given(st.floats(allow_nan=False, allow_infinity=False))
    def test_update_of_point_dimension(self, arg):
        """Update a random point: in one dimension"""

        # For every dimension
        for dim in ["i", "j", "k"]:

            # Create a copy of the current curve
            crv = deepcopy(self.crv)

            # Calculate parameters (Curve)
            _ = crv.s
            crv.fit_splines()

            # Calculate parameters (Curve2D)
            _ = crv.slopes
            _ = crv.normals

            # Update dimension of random point
            ix = np.random.randint(0, len(crv))
            setattr(crv[ix], dim, arg)

            # Grab new state
            state = crv.get_state()

            with self.subTest(dim):

                # Check that all values were reset
                check = all(
                    [value == None.__class__.__name__ for value in state.values()]
                )
                self.assertTrue(check)


class TestProperties(unittest.TestCase):
    """Test basic properties"""

    def setUp(self):
        # Create basic curve
        # self.crv = build_circle(np.linspace(0, 2.0 * np.pi, 200), ctype=Curve2D)
        pass

    """ Basic properties """
    # TODO: get item ie: singular, multiple, slices
    # TODO: set item ie: singular, multiple, slices
    # TODO: __repr__ test if strict definition is necessary

    """ Slopes & Normals """

    def test_normals(self):

        # Create a random wave

        # Calculate the normals at each point

        

    """ Curvature Methods """

    @given(
        st.integers(min_value=200, max_value=1000),
        st.floats(
            allow_nan=False, allow_infinity=False, min_value=1e-3, max_value=1e10
        ),
        st.tuples(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=1e-7, max_value=1e7
            ),
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=1e-7, max_value=1e7
            ),
        ),
    )
    def test_curvature(self, n: int, radius: float, center: Tuple[float, float]):
        """Verify curvature of a circle

        Args:
            n (int): number of points
            radius (float): length of radius
            center (tuple): x, y coordinates for shape center
            ctype (class): class type for shape
        """

        # Build a circle
        crv = build_circle(n, radius, center, ctype=Curve2D)

        # Compute curvature
        k = np.mean(crv.curvature())

        # Estimate average curvature as 1 / r
        goal = 1 / radius

        self.assertTrue(abs(k - goal) / goal <= 1e-3)

    @given(
        st.integers(min_value=500, max_value=1000),
        st.floats(
            allow_nan=False, allow_infinity=False, min_value=1e-3, max_value=1e6
        ),
        st.tuples(
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=1e-4, max_value=1e4
            ),
            st.floats(
                allow_nan=False, allow_infinity=False, min_value=1e-4, max_value=1e4
            ),
        ),
    )
    def test_evolute(self, n: int, radius: float, center: Tuple[float, float]):
        """Verify the evolute of a circle

        Args:
            n (int): number of points
            radius (float): length of radius
            center (tuple): x, y coordinates for shape center
            ctype (class): class type for shape
        """

        # Build a circle
        crv = build_circle(n, radius, center, ctype=Curve2D)

        # Compute evolute
        evl_x, evl_y = crv.evolute()

        # Estimate average position
        evl_x = np.percentile(evl_x, 50)
        evl_y = np.percentile(evl_y, 50)

        # Verify evolute is near center
        # Valid if less than 0.25% difference
        self.assertTrue(100 * abs(evl_x - center[0]) / center[0] <= 0.25)
        self.assertTrue(100 * abs(evl_y - center[1]) / center[1] <= 0.25)


# TODO: Line checks: normal if dot product is 0
