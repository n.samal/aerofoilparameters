import os
import sys

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d

# Add parent to path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from geometry import Point, Line, Line3D, Curve, Airfoil
from utilities import plot_extras, get_colors

# Tags for ease
OBJ = 'object'
KW = 'keyword'

''' Computation '''

def generate_point_at_angle(theta: float, phi: float, radius: float, show: bool=True):
    """Create a point at angle from the origin
    
    Use spherical coordinates to define a vector from the origin

    Args:
        theta (float): angle from vector to positive x-axis [radians]
        phi (float): angle from vector to positive z-axis [radians]
        radius (float): length of vector from origin
    Returns:
        Point: point at new angle
    References:
        https://en.wikipedia.org/wiki/Spherical_coordinate_system
        http://www.nabla.hr/VT-Vectors2Dand3DB2.htm
        https://mathinsight.org/spherical_coordinates
    """

    x = radius * np.sin(theta) * np.cos(phi)
    y = radius * np.sin(theta) * np.sin(phi)
    z = radius * np.cos(theta)

    # Create line
    line = Line3D([Point(0, 0, 0), Point(x, y, z)])

    if show:

        # Create axes
        axes = plt.gca()

        # Create
        axes.plot()
    
    return line.stop


def get_direction_angles(vec1: np.array, vec2: np.array=None):
    """Use direction cosines to calculate ratios / angles

    Args:
        vec1 (np.array): first vector
        vec2 (np.array): second vector
    Returns:
        np.array: angles in each direction [rad]
    References:
        https://en.wikipedia.org/wiki/Direction_cosine
        http://mathworld.wolfram.com/DirectionCosine.html
        http://www.euclideanspace.com/maths/geometry/rotations/directionCosines/index.htm
        https://brilliant.org/wiki/dot-product-direction-cosines/
    Todo:
        verify validity of reference to another vector
    """

    if vec2 is None:

        # Compute the directions and angles
        mag_v1 = np.sqrt(np.sum(np.square(vec1)))
        dirs = vec1 / mag_v1
    else:

        # Compute magnitude
        mag_v2 = np.sqrt(np.sum(np.square(vec2)))

        # Compute angles
        dirs = (vec1 * vec2) / (mag_v1 * mag_v2)

    return np.arccos(dirs)

''' Plotting '''

def plot2D(data: dict, var_x: str='i', var_y: str='j'):
    """Plot object attributes in 2D

    Args:
        data (Dict[obj]): data and plotting arguments
        var_x (str): dimensional data for x-axis
        var_y (str): dimensional data for y-axis
    Returns:
        None
    """

    print(' Plotting in 2D')

    # Plot points
    axes = plt.gca()
    kwargs_sct = {'marker': 'o', 'markersize': 15}
    kwargs_lin = {'linewidth': 6}

    # Plot lines
    for key in data.keys():

        # Merge custom settings with default style
        if KW in data[key]:

            if isinstance(data[key][OBJ], Point):
                kwargs = {**kwargs_sct,  **data[key][KW]}
            else:
                kwargs = {**kwargs_lin,  **data[key][KW]}

        # Plot away
        axes.plot(getattr(data[key][OBJ], var_x),
                  getattr(data[key][OBJ], var_y),
                  **kwargs)

        kwargs = {}

    plot_extras(axes, xlabel=var_x, ylabel=var_y, equal=True)


def plot3D(line_nrm, lines: dict):
    """Plot line in 3D

    Args:
        line_nrm (Line): normal line
        line_rot (Dict[Line]): rotate lines
    Returns:
        None
    References:
        https://matplotlib.org/mpl_toolkits/mplot3d/tutorial.html
    """

    # Define figure and axes
    axes = plt.axes(projection='3d')

    kwargs_sct = {'s': 15}
    kwargs_lin = {'linewidth': 6}

    # Plot points
    axes.scatter3D(line_nrm.start.i, line_nrm.start.j, line_nrm.start.k, c='red', *kwargs_sct)
    axes.scatter3D(line_nrm.stop.i, line_nrm.stop.j, line_nrm.stop.k, c='grey', *kwargs_sct)
    # axes.scatter3D(line_rot.stop.i, line_rot.stop.j, line_rot.stop.k, c='green', *kwargs_sct)

    # Plot lines
    axes.plot(line_nrm.i, line_nrm.j, line_nrm.k, c='grey', linewidth=4)

    for key in lines.keys():
        axes.plot(lines[key].i, lines[key].j, lines[key].k, c='green', label=key, linewidth=4)

    # Compute cross products
    unit_i = np.array([1, 0, 0])
    unit_j = np.array([0, 1, 0])
    unit_k = np.array([0, 0, 1])

    # Create line about origin
    origin = Point(0, 0, 0)
    axis_i = Line3D([origin, Point(*unit_i)])
    axis_j = Line3D([origin, Point(*unit_j)])
    axis_k = Line3D([origin, Point(*unit_k)])

    # Transform coordinate system to align with normal vector

    # Modified coordinate system
    # axis_i = Point(*np.cross(unit_i, line_nrm.stop.coords))
    # axis_j = Point(*np.cross(unit_j, line_nrm.stop.coords))
    # axis_k = Point(*np.cross(unit_k, line_nrm.stop.coords))

    # axes.plot(axis_i.i, axis_i.j, axis_i.k, c='red', label='Unit i', linewidth=4)
    # axes.plot(axis_j.i, axis_j.j, axis_j.k, c='orange', label='Unit j', linewidth=4)
    # axes.plot(axis_k.i, axis_k.j, axis_k.k, c='lightgreen', label='Unit k', linewidth=4)

    # Add labels
    axes.set_title('Vector Manipulation')
    axes.set_xlabel('x')
    axes.set_ylabel('y')
    axes.set_zlabel('z')

    axes.set_xlim3d(0,15)
    axes.set_ylim3d(0,15)
    axes.set_zlim3d(0,15)
    # axes.axis('equal')

    plt.show()

''' Test Cases '''

def test_cases_directions():
    """Test cases for a vector angles

    Args:
        None
    Returns:
        None
    References:
        https://www.engineeringmathgeek.com/direction-cosines-of-vectors/
    """

    # Vectors: test cases
    v1 = np.array([3, 7, -4])
    print(np.rad2deg(get_direction_angles(v1)))

    lines = {}

    plot2D(line_nrm, lines, var_x='i', var_y='j')
    plot2D(line_nrm, lines, var_x='i', var_y='k')
    plot2D(line_nrm, lines, var_x='j', var_y='k')

    v1 = np.array([1, -5, -8])
    print(np.rad2deg(get_direction_angles(v1)))

    v1 = np.array([6, -2, 12])
    print(np.rad2deg(get_direction_angles(v1)))


def test_vector_rotations():

    print(' Defining point and vectors')

    # Surface position and normal
    p_xyz = Point(-0.524001479, 13.05234241, -1.539136887)
    p_nrm = Point(0.765336514, 0.004911592, -0.643611133)

    # Place point at end of normal
    p_xyz_n = p_xyz + p_nrm

    # Normal line
    line_nrm = Line3D([p_xyz, p_xyz_n])

    data = {
             'Normal': {OBJ: line_nrm, KW: {'label': 'Normal', 'color': 'green'}},
             'i': {OBJ: line_nrm.rotate_i(np.deg2rad(90), center=p_xyz), KW: {'label': 'Normal: rotated i', 'color': 'firebrick'}},
             'j': {OBJ: line_nrm.rotate_j(np.deg2rad(90), center=p_xyz), KW: {'label': 'Normal: rotated j', 'color': 'orange'}},
             'k': {OBJ: line_nrm.rotate_k(np.deg2rad(90), center=p_xyz), KW: {'label': 'Normal: rotated k', 'color': 'yellowgreen'}}
            }

    # Plot
    plot2D(data, var_x='i', var_y='j')
    # plot2D(data, var_x='i', var_y='k')
    # plot2D(data, var_x='j', var_y='k')
    # plot3D(line_nrm, lines)


if __name__ == "__main__":

    print(__file__)

    # test_cases_directions()
    test_vector_rotations()

    print(' -> complete.')
