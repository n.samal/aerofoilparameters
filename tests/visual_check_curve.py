import numpy as np
from matplotlib import pyplot as plt
import os
import sys
from typing import Callable, Tuple

# Add parent to path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)))

from geometry import Point, Curve, Curve2D
from utilities import plot_extras, get_colors
from utilities_curve import (
    build_circle,
    build_ellipse,
    build_polygon,
    build_rose,
    build_sine_wave,
)

""" Curve Methods: Transforms """


def check_reflection(crv: Curve, method: str, n: int = 30):
    """Visually check the reflection method

    Args:
        crv (Curve): curve to transform
        method (str): method to apply
        n (int): number of transformations
    """

    # Create basic axis
    axes = plt.gca()
    kwargs = {"ms": 4, "linewidth": 3, "marker": "o"}

    colors = get_colors(n)

    for ix, val in enumerate(np.linspace(0, 1, n)):

        # Grab appropriate coordinates
        if method == "reflect_i":
            x = "i"
            y = "j"
            center_i = Point(val, 0, 0)
        elif method == "reflect_j":
            x = "i"
            y = "j"
            center_i = Point(0, val, 0)
        elif method == "reflect_k":
            x = "k"
            y = "i"
            center_i = Point(0, 0, val)
        else:
            raise ValueError(
                f"Unexpected method type: {method}, expecting 'reflect_i, reflect_j or reflect_k'"
            )

        # Transform curve and plot
        crv_i = getattr(crv, method)(center=center_i)
        axes = crv_i.plot(axes, x, y, color=colors[ix], **kwargs)

    # Add extras
    plot_extras(axes, equal=True, title=method + " check", xlabel=x, ylabel=y)


def check_rotation(crv: Curve, method: str, n: int = 50):
    """Visually check rotation of a curve
    
    Args:
        crv (Curve): base curve
        method (str): transformation method
        n (int): number of points to check
    """

    print(f" Checking rotation: {method}")

    # Grab appropriate coordinates
    if method == "rotate_i":
        x = "j"
        y = "k"
    elif method == "rotate_j":
        x = "i"
        y = "k"
    elif method == "rotate_k":
        x = "i"
        y = "j"
    else:
        raise ValueError(
            f"Unexpected method type: {method}, expecting 'rotate_i, rotate_j or rotate_k'"
        )

    # Plot the curve
    axes = crv.plot(var_x=x, var_y=y, marker="o", label="Original")

    # Generate colors and random point
    colors = get_colors(n)
    pt = Point(*np.random.rand(3))

    # Create random rotation
    for ix, rot in enumerate(np.linspace(0, np.pi * 2.0, n)):

        # Create new curve and plot
        crv_i = getattr(crv, method)(rot, pt)
        axes = crv_i.plot(
            axes, var_x=x, var_y=y, marker="o", label=None, color=colors[ix]
        )

    plot_extras(axes, title=method, xlabel=x, ylabel=y, equal=True, show=True)


def check_scale(crv: Curve, n: int = 20):
    """Visually check the scaling of a curve
    
    Args:
        crv (Curve): base curve
        n (int): number of points to check
    """

    # Plot the curve
    axes = crv.plot(color="black", marker="o", label="Original")

    # Generate colors and random point
    colors = get_colors(n)
    pt = Point(*np.random.rand(3))

    # Create random rotation
    for ix, scl in enumerate(np.linspace(0.50, 2.0, n)):

        # Create new curve and plot
        crv_i = getattr(crv, "scale")(scl, pt)
        axes = crv_i.plot(axes, marker="o", label=None, color=colors[ix])

    plot_extras(axes, equal=True, show=True)


def check_translation(crv: Curve, dim: str, n: int = 20):
    """Visually check the scaling of a curve
    
    Args:
        crv (Curve): base curve
        dim (str): dimension for translation
        n (int): number of points to check
    """

    # Plot the curve
    axes = crv.plot(color="black", marker="o", label="Original")

    # Generate colors and random point
    colors = get_colors(n)

    # Create random base offset
    pt = Point(*np.random.rand(3))
    delta = np.zeros(3)
    jx = crv.get_dim_index(dim)

    # Create series of offsets
    for ix, scl in enumerate(np.linspace(0, 1, n)):

        # Modify dimension of interest
        delta[jx] = scl

        # Create new curve and plot
        crv_i = getattr(crv, "translate")(*delta)
        axes = crv_i.plot(axes, marker="o", label=None, color=colors[ix])

    plot_extras(
        axes, xlabel="i", ylabel="j", title=f"Translation: {dim}", equal=True, show=True
    )


""" Curve Methods: Utility """


def check_interpolation(crv: Curve):
    """Visually check the interpolation method"""

    print(" Checking interpolation")
    crv2 = crv.interpolate_by_s(show=False)

    # Plot both curves
    axes = crv.plot(label="Original", color="grey")
    axes = crv2.plot(
        axes, label="Interpolation", color="green", linestyle="-.", marker="o"
    )
    plot_extras(axes, equal=True, show=True)


def check_reverse(crv: Curve):
    """Visually check the reversal method
    
    Todo:
        add annotation
    """

    print(" Checking reversal")
    crv2 = Curve(crv.points)
    crv2.reverse()

    # Plot both curves
    axes = crv.plot(label="Original", color="grey")

    axes = crv2.plot(axes, label="Reverse", color="green", linestyle="-.", marker="o")
    plot_extras(axes, equal=True, show=True)


""" Curve2D Methods: Vectors """


def check_tangencies(crv: Curve2D, **kwargs):
    """Visually check the tangencies around a curve"""

    # Plot the curve and add tangencies
    axes = crv.plot(marker="o", label="Original")
    axes = crv.draw_tangent_lines(1, axes, marker="o", **kwargs)
    plot_extras(axes, equal=True, show=True)


def check_normals(crv: Curve2D, **kwargs):
    """Visually check the normals around a curve"""

    # Plot the curve and add tangencies
    axes = crv.plot(marker="o", label="Original")
    axes = crv.draw_normal_lines(1, axes, marker="o", **kwargs)
    # axes = crv.draw_normal_lines(np.random.rand(len(crv)), axes, marker='o')
    plot_extras(axes, equal=True, show=True)


""" Curve2D Methods: Curvature """


def check_curvature_methods(crv: Curve2D):
    """Check curvature values for several methods"""

    # Get curvature method
    # methods = ['analytical', 'polynomial', 'chord', 'randy']
    methods = ["spline_t", "spline_t_pw", "polynomial"]
    offset = 2

    # Generate axis
    axes = plt.gca()

    # Plot curvatures
    for method in methods:

        k = crv.curvature(method, offset)

        # Plot curvature
        label = f"Method: {method} Offset: {offset}"
        axes.plot(k, label=label, linewidth=4)

    # Add plot extras
    plot_extras(axes)


def check_curvature(crv: Curve2D, **kwargs):
    """Visually check the curvature around a curve"""

    # Plot the curve and add tangencies
    axes = crv.plot(marker="o", label="Original")
    axes = crv.draw_curvature_normal_lines(-1, axes, marker="o", **kwargs)
    # axes = crv.draw_normal_lines(np.random.rand(len(crv)), axes, marker='o')
    plot_extras(axes, equal=True, show=True)


def check_evolute(crv: Curve2D, **kwargs):
    """Visually check the center of curvature around a curve"""

    # Plot the curve
    axes = crv.plot(marker="o", label="Original")
    axes = crv.draw_evolute(axes, **kwargs)
    plot_extras(axes, equal=True, show=True)


# todo: create tests for point updates in Curves
# todo: verify average curvature of a circle


""" Curve: Update """


def check_curve_update(crv: Curve):
    """Check if the curve gets an update"""

    print(crv)

    state = crv.get_state()

    # Grab out normalized values
    s = crv.s
    state = crv.get_state()

    # Create some curve
    print("\nModifying value: point")
    # crv[0] = Point(1, 2, 3)
    crv.start = Point(1, 2, 3)
    print(crv.start)

    # Check state: ie: variables reset
    state = crv.get_state()
    quit()

    # print('Modifying value: single dimension')
    # crv[0].i = 1
    # crv[0].j = 2
    # crv[0].k = 0
    # print(crv.start)

    # Grab out normalized values
    s = crv.s

    print(crv.get_state())

    # todo: create update boolean for after update checks

    # check after update a single point
    # check after updating multiple points
    #  check that Curve requires an update
    #  check that Curve2D requires an update
    #  check after updating i
    #  check after updating j
    #  check after updating k
    #  check after updating ijk
    #  check after reversing curve


if __name__ == "__main__":

    print(__file__)

    # Build several shapes
    # build_polygons(3, 10)

    # Build singular shape
    kwargs = {
        "radius": np.random.random() * np.random.randint(1e-4, 1e4),
        "center": (
            np.random.random() * np.random.randint(1e-4, 1e4),
            np.random.random() * np.random.randint(1e-4, 1e4),
        ),
        "n": np.random.randint(500, 1000),
    }
    crv = build_circle(**kwargs)

    # crv = build_ellipse(2.5, 4)
    # crv = build_polygon(3)
    # crv = build_rose(2.75, n=1000)
    # crv = build_sine_wave(np.linspace(0, 2.0*np.pi, 200))
    # crv = build_cosine_wave(np.linspace(0, 2.0*np.pi, 200))
    # crv = build_tangent_wave(np.linspace(-4.0*np.pi, 4.0*np.pi, 1000))

    # check_curve_update(crv)

    # Plot the curve
    axes = crv.plot(marker="o", label="Original")
    plot_extras(axes, equal=True, show=True)

    # Distribution checks
    # crv.angle(show=True)
    # crv.curvature(show=True)

    # Checks
    # check_interpolation(crv)
    # check_reverse(crv)
    # check_curvature_methods(crv)
    check_normals(crv)
    # check_tangencies(crv)
    # check_curvature(crv)
    # check_evolute(crv)

    # Transformation checks: reflect
    # check_reflection(crv, 'reflect_i', n=20)
    # check_reflection(crv, 'reflect_j', n=20)
    # check_reflection(crv, 'reflect_k', n=20)

    # Transformation checks: rotate
    # check_rotation(crv, 'rotate_i', n=20)
    # check_rotation(crv, 'rotate_j', n=20)
    # check_rotation(crv, 'rotate_k', n=20)

    # Transformation checks: Scale
    # check_scale(crv)

    # Transformation checks: translate
    # check_translation(crv, 'i', n=20)
    # check_translation(crv, 'j', n=20)
    # check_translation(crv, 'k', n=20)
    print("Complete!")
