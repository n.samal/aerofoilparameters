import numpy as np
from typing import Callable, Tuple

from geometry import Point, Curve, Curve2D
from utilities import plot_extras

""" Shape Generation: Polar """


def build_circle(n: int, radius: float, center: Tuple[float, float] = None, ctype=None):
    """Build a circlular shape

    Args:
        n (int): number of points
        radius (float): length of radius
        center (tuple): x, y coordinates for shape center
        ctype (class): class type for shape
    Returns:
        Curve: points defining a closed polygon (ends on the start point)
    Todo:
        Modify theta to start point at the top
    """

    if center is None:
        center = (0, 0)

    if ctype is None:
        ctype = Curve2D

    points = []

    # Break the theta space into pieces
    for theta in np.linspace(0, np.pi * 2, n + 1):

        # Convert to cartesian and store
        x = radius * np.cos(theta) + center[0]
        y = radius * np.sin(theta) + center[1]
        points.append(Point(x, y, 0))

    return ctype(points)


def build_ellipse(a: float, b: float, n: int = 200, ctype=None):
    """Build an ellipse

    Args:
        a (float): radius of x axis
        b (float): radius of y axis
        n (int): number of points about parametric evaluation range
        ctype (class): class type for shape
    Returns:
        Curve: points defining an ellipse
    References:
        https://en.wikipedia.org/wiki/Ellipse
    """

    # TODO: add ellipse center
    # TODO: modify circle to utilize elliptical function

    print(" Building ellipse")

    if ctype is None:
        ctype = Curve2D

    points = []

    # Break the theta space into pieces
    for theta in np.linspace(0, np.pi * 2, n + 1):

        print(f"  theta: {theta:.3f}")

        # Convert to cartesian and store
        x = a * np.cos(theta)
        y = b * np.sin(theta)
        points.append(Point(x, y, 0))

    return ctype(points)


def build_rose(k: float, a: float = 1.0, n: int = 200, ctype=None):
    """Build a rose curve

    Args:
        k (float): frequency scalar
        a (float): amplitude scalar
        n (int): number of points in parametric evaluation range
        ctype (class): class type for shape
    Returns:
        Curve: points defining a rose
    References:
        https://en.wikipedia.org/wiki/Rose_(mathematics)
        http://mathworld.wolfram.com/Rose.html
    """

    print(" Building a rose")

    if ctype is None:
        ctype = Curve2D

    points = []

    # Break the theta space into pieces
    for theta in np.linspace(0, np.pi * 2, n + 1):

        print(f"  theta: {theta:.3f}")

        # Polar form
        r = a * np.cos(k * theta)

        # Convert to cartesian
        x = r * np.cos(theta)
        y = r * np.sin(theta)

        # Store
        points.append(Point(x, y, 0))

    return ctype(points)


""" Shape Generation: Polygons """


def build_polygon(n_sides: int = 200, radius: float = 1.0, n: int = 200, ctype=None):
    """Build a polygon

    Args: 
        n_sides (int): number of polygon sides
        n (int): number of points
        radius (float): amplitude scalar
        ctype (class): class type for shape
    Returns:
        Curve: points defining a rose
    References:
        https://math.stackexchange.com/questions/41940/is-there-an-equation-to-describe-regular-polygons
    Todo:
        - review proposed equations in reference
    """

    if ctype is None:
        ctype = Curve2D

    print(f" Building a polygon: {n_sides} Sides")

    points = []
    theta = np.linspace(0, np.pi * 2, n_sides + 1)

    # Generate vertices
    for ix in range(n_sides):

        # On the last side
        if ix == n_sides - 1:

            # Stop on the first vertex
            ix_start = ix
            ix_stop = 0
            endpoint = True

        else:
            # Use adjacent indices
            ix_start = ix
            ix_stop = ix + 1
            endpoint = False

        # Start and stop point
        start_x = radius * np.cos(theta[ix_start])
        start_y = radius * np.sin(theta[ix_start])

        stop_x = radius * np.cos(theta[ix_stop])
        stop_y = radius * np.sin(theta[ix_stop])

        """ Line between vertices """

        # Slopes
        mx = stop_x - start_x
        my = stop_y - start_y

        # Build a line between points
        for t in np.linspace(0, 1, n // n_sides, endpoint):

            x = start_x + mx * t
            y = start_y + my * t

            points.append(Point(x, y, 0))

    return ctype(points)


def build_polygons(start: int = 3, stop: int = 10):
    """Build a series of polygons and visually verify

    Args:
        start (int): number of sides to start loop
        stop (int): number of sides to end loop
    Returns:
        None
    """

    for n in range(start, stop + 1):

        # Create polygons
        crv = build_polygon(n, 1.5)

        # Show plot
        axes = crv.plot(label=f"Polygon: {n}")
        plot_extras(
            axes, xlabel="Dimension i", ylabel="Dimension j", equal=True, show=True
        )


""" Shape Generation: Waves """


def build_wave(
    func: callable,
    itrb: iter,
    amplitude: float = 1.0,
    freq: float = 1.0,
    shift: float = 0.0,
    ctype=None
):
    """Build general wave using the provided trigonometric function

    y = A*func(freq * t - shift)

    Args:
        func (float): trig function to apply
        itrb (iter): iterable for function evaluation
        amplitude (float): height of peak amplitude without an offset
        freq (float): wave frequency
        shift (float): phase shift
        ctype (class): class type for shape
    Returns:
        Curve: points defining the evaluated trig function
    References:
        https://en.wikipedia.org/wiki/Phase_(waves)
        https://en.wikipedia.org/wiki/Trigonometric_functions
    """

    if ctype is None:
        ctype = Curve2D

    points = []

    # Iterate over iterable
    for i in itrb:

        # Evaluate function
        j = amplitude * func(freq * i - shift)
        points.append(Point(i, j, 0.0))

    return ctype(points)


def build_cosine_wave(*args, **kwargs):
    """Build a cosine wave

    y = A*cos(freq * t - shift)

    Args:
        itrb (iter): iterable for function evaluation
        amplitude (float): height of peak amplitude without an offset
        freq (float): wave frequency
        shift (float): phase shift
    Returns:
        Curve: points defining an evaluated cosine wave
    """
    return build_wave(np.cos, *args, **kwargs)


def build_sine_wave(*args, **kwargs):
    """Build a sine wave

    y = A*sin(freq * t - shift)

    Args:
        itrb (iter): iterable for function evaluation
        amplitude (float): height of peak amplitude without an offset
        freq (float): wave frequency
        shift (float): phase shift
    Returns:
        Curve: points defining an evaluated sine wave
    """
    return build_wave(np.sin, *args, **kwargs)


def build_tangent_wave(*args, **kwargs):
    """Build a tangent wave

    y = A*tan(freq * t - shift)

    Args:
        itrb (iter): iterable for function evaluation
        amplitude (float): height of peak amplitude without an offset
        freq (float): wave frequency
        shift (float): phase shift
    Returns:
        Curve: points defining an evaluated sine wave
    """
    return build_wave(np.tan, *args, **kwargs)
